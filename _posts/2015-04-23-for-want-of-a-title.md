---
layout: post
title: "For Want of a Title"
modified:
categories:
excerpt:
tags: []
image:
  feature:
date: 2015-04-23T15:52:51-07:00
---

The WNBA's [Glory Johnson and Brittney Griner were arrested on charges of assault and disorderly conduct](http://www.autostraddle.com/glory-johnson-and-brittney-griner-arrested-charged-with-assault-and-disorderly-conduct-287408/). Both Johnson and Griner have since been released. A few media outlets were quick to paint this as an intimate partner violence situation, which feels inexact for a number of reasons. First:  BG and GloJo are physical equals. sure, BG is taller, but on the surface, it's hard to imagine the unequal power relations that are a frequent indicator of spousal/partner abuse being at play in this relationship.

Second: there's a spectre of "butch-as-aggressor" framing going on in the media. Many articles only mention BG in the headline, a construction that elides Johnson's involvement. You see BG's mugshot more than you see Glory Johnson's. While it's true that Griner has the higher profile, it seems to me that BG is being given the perpetrator edit.

[Autostraddle](http://www.autostraddle.com/glory-johnson-and-brittney-griner-arrested-charged-with-assault-and-disorderly-conduct-287408/) is one of the few media outlets to refrain from using mugshots in their coverage, and as far as I know, they're the only publication that included Johnson's name in the headline. Their reporting has been fair and nuanced, which I'm ashamed to say, wasn't something I expected to see on Autostraddle. Kaelyn, the author of the Autostraddle piece, sounded off in the comments about assumptions people were making about the nature of the Griner/Johnson relationship, and [her comment](http://www.autostraddle.com/glory-johnson-and-brittney-griner-arrested-charged-with-assault-and-disorderly-conduct-287408/#comment-489871) showed the kind of sensitivity and thoughtfulness you wish all media would display[^1].

This is a difficult issue, and my status as the ultimate Griner stan may make you take this opinion with a  grain of salt, which is only fitting, considering I wasn't there when the incident happened. I had such high hopes for this couple, and I hope they're able to get whatever help they need to make it through this situation.

---

Thanks to everyone who has signed up for [The Librarian Cabal](http://librariancabal.wordpress.com). I started the channel so library folk would have a safe, open place to talk about All Things Library without fear of recrimination. The Cabal has a Code of Conduct in place, and I hope everyone will honor the spirit and the letter of the CoC. It'll probably be a little tough to enforce. I'm committed to try, because like Anil Dash [said](http://dashes.com/anil/2011/07/if-your-websites-full-of-assholes-its-your-fault.html), "If your website's full of assholes, it's your fault."

Additional thanks to everyone who shared or commented on my [Bridging the Experience Gap](http://cecily.info/2015/04/11/bridging-the-experience-gap/) article. I think some great things will come out of the conversations that sprang up, including a tweet chat, and possibly a mailing list (if I can find an alternative to Google groups, that is). I'm not sure how to move on as an interest group/sub-group of a national library association when many of the people who replied are outside of the United States, so if you have any ideas about how to make that work, I'm happy to listen.

---

I'm contemplating moving away from [Jekyll](http://jekyllrb.com) and back to WordPress to power this site. Jekyll is powerful, and [learning to work with it](http://cecily.info/2014/08/28/finding-myself-through-code/) has increased my comfort and familiarity with Ruby programming. But to tell the truth, there are times that I really miss a web-based CMS, or one that comes equipped with a robust mobile app that supports blogging from anywhere. I've been slowly moving some posts over to a WordPress site I've kept around for just such an occasion, but I probably won't flip the switch for a few weeks yet. I'd like the switch to be as seamless as possible, so I'm taking some time to reformat entries and ensure that URLs stay the same between the two platforms.

[^1]: It was also the first time I'd heard the term "common couple violence", so additional kudos to AS for teaching me something new today.
