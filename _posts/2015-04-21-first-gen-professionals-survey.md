---
layout: post
title: "First Gen Professionals Survey"
modified:
categories: 
link: https://docs.google.com/forms/d/1q3FyctdeCdGP19hQBQzl4qvcLEztz_y4tYOB_LtKvH4/viewform
date: 2015-04-21T19:14:24-07:00
---

I'd like to find out how much interest there is in a network first generation library professionals, and to find out what shape it should take. If you have a few minutes, please consider taking the survey. 

(A special hat-tip to [Ellie Collier](https://about.me/elliehearts) for putting the survey together.)