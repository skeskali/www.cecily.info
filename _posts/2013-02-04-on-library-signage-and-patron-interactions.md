---
title: On Library Signage and Patron Interactions
layout: post
share: true
comments: true
date: 2013-02-04
---
<a href="http://randomaccessmazar.wordpress.com/" title="Rochelle Mazar" target="_blank">Rochelle </a>is on fire <a href="http://randomaccessmazar.wordpress.com/2013/02/04/what-i-learned-about-librarianship-from-the-signage-on-the-underground/" title="What I Learned about Librarianship From the Signage on the Underground" target="_blank">with this post</a>: 

> Librarians have a tendency to behave as if patrons walk through the door needing to know practically everything about their journey before they take their first step. We haul out the maps, give advice about the weather and what footwear they need for the first half, and trace the entire experience out before they get past the turnstile. We may never see that patron again; we’d better make sure they’re well-prepared. For each and every leg of the journey. Then we leave them to their own devices, unless they want to seek us out again. What if we focused on reducing confusion and anxiety if all of our patron interactions by guiding their decisions in small pieces, manageable ones, rather than infodumping right at the start?

A couple of years ago I participated in a working group that was charged with making recommendations for new directional signage in the library. As a result, we now have these rather large, 8ft. tall black pillars placed right in front of our escalators; depending on the floor you&#8217;re on at the time, the pillars obscure the public&#8217;s sight lines to the information desk (and our sight lines to the escalators). Even though we did the best we could with what we had, I wonder if our efforts would have been better spent on reorganizing the floors, or, as Rochelle suggests, focusing more on reducing anxiety.

