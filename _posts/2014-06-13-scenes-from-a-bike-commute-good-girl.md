---
title: 'Scenes From a Bike Commute: Good Girl'
layout: post
share: true
comments: true
date: 2014-06-13
summary: Today, while on my bike...
---
A middle-aged woman with a riot of luscious gray curls crossed the street in front of me as I waited at the stop sign. She was walking a small, skittish dog, and as she drew nearer I heard her say &#8220;Good girl!&#8221;

&#8220;Look at you, all colour-coordinated!&#8221; she said to me as she crossed. I&#8217;m wearing a white cardigan over a turquoise sweater, blue jeggings rolled up to my calves, and [mint-green and white New Balance 501s][1]. You already know [what my bike looks like][2].

She continued, &#8220;That&#8217;s really part of it, isn&#8217;t it? If you&#8217;re going to have to ride your bike, you might as well look good while doing it.&#8221; I laughed in agreement, and pedaled away.

But I still don&#8217;t know if the &#8220;good girl&#8221; was for me or for her dog.

 [1]: http://www.newbalance.com/New-Balance-501/WL501H,default,pd.html "I love these shoes."
 [2]: https://www.flickr.com/photos/cecily/11092302066/
