---
layout: post
title: "You Are Beyond Compare"
comments: true
share: true
modified:
categories: 
excerpt:
tags: []
image:
  feature:
date: 2014-09-12
---

## Comparison Shopping
I’ve just signed up for Lauren Bacon and Tanya Geisler’s [Beyond Compare](http://www.laurenbacon.com/beyond-compare/) course. The focus of the program is to help women step away from comparing ourselves to other people, and “into celebrating each other’s successes.”

When I initially read through the course description, I didn’t think it would be relevant. “I don’t have a problem with comparing myself to other women,” I said. “What’s good for them is good for them, and what’s good for me is good for me.” But when I took some time to think about it, I realized that [I do have a problem with comparing myself to other women](/2013/12/30/of-what-had-i-ever-been-afraid/), specifically:

- Whenever I think I’m not polished enough
- Whenever I think I’m not professional enough
- Whenever I think I don’t have the right skills to make it into higher-level positions at work
- Whenever I say to myself “I wish *I* could do that…”, no matter the skill or event
- Whenever I don’t share my opinions or ideas at work for fear that they’ll be mocked, dissected, or worse, discredited

## How Can I Be Better?
I’ve spent the last few years working out how I could be a better friend, employee, manager, *person*. I haven’t really reached any grand conclusions, and honestly, I feel that for every step forward I take in my development I’m still taking several steps back. We all stumble, but strength comes from our ability to pull ourselves up to standing once more. This is the step I need the most help with.

Lauren and Tanya believe it’s human nature to compare ourselves to other people, and challenging ourselves to do and be better is a fundamental part of growth and development. Where we tend to fall short is when we let these comparisons consume us and rob us of the ability to celebrate ourselves and our own successes. 

Competitiveness and insecurity caused me to make some really unwise professional and personal choices. Maybe if I’d had a better handle on the my strengths, I wouldn’t have walked away from a job at a software development company with no notice. If I’d believed my plurality of identities were assets instead of liabilities, I might have spared myself from the psychological and financial repercussions of a decision made in anger. If I hadn’t devoted so much energy to thinking I could never measure up to her standards, I might have felt more equipped to go to my former manager and tell her how deeply her critical style cut me.

## Get the Starter Kit

If you’re tired of looking outward at everyone else and finding yourself wanting, consider picking up the (free) [Beyond Compare Starter Kit](http://www.laurenbacon.com/beyond-compare/). Whether you work alone or as part of the Beyond Compare program, the Starter Kit will guide you through a series of exercises that challenge you to go deeper and promise to transform your way of thinking. You can get the kit by giving Lauren and Tanya your email address, and they’ll send you a link to the PDF download. 

I’m excited to start this process, and I’m looking for a few other kindred spirits to join me on this journey. If you’re interested in forming a Beyond Compare discussion group, respond in the comments below. I think we would all benefit from being honest, being vulnerable, and being unafraid to tell the truth in a supportive atmosphere. When the heart opens, learning and love pour in.