---
layout: post
title: "Freedom Libraries"
comments: true
share: true
date: 2015-02-24T20:40:07-08:00
---
<blockquote class="twitter-tweet" lang="en"><p>How do we support the person probably without a library background who winds up running a small library for $7.25 an hour? How do we help?</p>&mdash; dolly m (@loather) <a href="https://twitter.com/loather/status/570288098950696960">February 24, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

After seeing this tweet from Dolly Moehrle and after reading Jason Griffey's [Poverty, Libraries, Jobs, Me ](http://jasongriffey.net/wp/2015/02/23/poverty-libraries-jobs-me/), I started thinking about organizing a grassroots movement that would support perpetually underfunded, understaffed small and/or rural libraries. For whatever reason -- maybe it's the residual effect of the Oscars -- I thought that a sustained movement that was based around a [Freedom Summer](http://mlk-kpp01.stanford.edu/index.php/encyclopedia/encyclopedia/enc_freedom_summer_1964/) concept might work.

(This is a very rough, stream of consciousness blog post, but I want to capture the basic ideas before they all slip away.)

## Freedom Summer

<iframe width="640" height="360" src="https://www.youtube.com/embed/_MR3HEySXUY" frameborder="0" allowfullscreen></iframe>

Over the course of 10 weeks in 1964, more than 1,000 community activists, civil rights organizations, students, clergy and laypeople organized a voter registration drive in Mississippi. Teams of volunteers descended on Mississippi in an attempt to dismantle one of the more pernicious manifestations of white supremacy - suppressing the right to vote. Though only 1200 people were ultimately registered, Freedom Summer remains a watershed moment in American history.

What if librarians, library associations, and other library workers organized a similar effort? MLIS students could be our community organizers, large library associations could provide financial and operational support (mailing, communications, logistical support), and librarians from around North America could volunteer their time to help struggling libraries with whatever these libraries needed.

It's important that this effort is sustainable, which is why I've backed off my original idea of some sort of crowd-sourced funding. It's also important that this not come off as a group of do-gooder outsiders who think they know what's best for local communities. Our first order of business would be to listen to the workers in these libraries to see what sort of help they need, and to make sure that those of us who have the time and resources could not only supply the help in crisis moments, but could be counted on to develop ongoing relationships with these library workers and their communities. We don't want to put a bow on something and disappear. That isn't helpful, and doesn't do much beyond making us feel better about ourselves.

## Next Steps
* Strategizing
* Building a team of organizers
* Contacting rural libraries and/or state library associations to identify libraries in need
* Raising awareness witih library assocations, library schools, etc.
* Putting together a plan of action -- I wouldn't even *begin* to know how to do this...

Your thoughts and ideas are what will help make this idea a reality, so feel free to comment below, or [contact me on Twitter](http://twitter.com/skeskali).
