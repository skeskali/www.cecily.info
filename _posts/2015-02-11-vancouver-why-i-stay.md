---
layout: post
title: "Vancouver - Why I Stay"
comments: true
share: true
modified:
categories:
excerpt:
tags: []
image:
  feature:
date: 2015-02-11T00:19:37-08:00
---
(Inspired by The Bold Italic’s [“What Keeps You In SF?”](http://www.thebolditalic.com/articles/6827-anonymous-answers-to-what-keeps-you-in-sf-))

It’s so expensive I can’t afford to move anywhere else.

My union job. If you’ve ever had a union job, you know how hard it is to give up that security.

When the fall fog banks roll in and envelop the city in cotton wool. The mournful melancholy of a [distant foghorn](http://youtu.be/C0c0TDlRMxI) momentarily makes my hair stand on end, but settles into a comforting bass note as the hours pass.

The damp chill suits my overall mood far more than blisteringly hot sunny days ever could.

The long late-spring evenings where it doesn’t get dark until almost 11:00pm

Really, _really_ good pho.

Incredible craft breweries around just about every corner. Vancouver turned me into a beer lover.

The way the [9 O’Clock gun](http://youtu.be/Dhf4c620FEA) echoes off the mountains on a clear winter night.

Ethnic diversity that goes beyond a black/white axis.

Living in a place where feeling like a minority isn’t (widely) considered to be a limitation.

The [Pineapple Express](https://en.wikipedia.org/wiki/Pineapple_Express) is perfect for hunkering down inside your comfortable, warm, postage-stamp sized apartment while the wind and rain hammer your windows.

Living so far away from the place I grew up in allows me to truly be the person I’ve always wanted to be. It also gives me the freedom to reinvent that person whenever the mood strikes.

Always knowing which way is North, thanks to the mountains.

Playing “spot the landmark” while watching Sci-Fi/Fantasy/Adventure shows becomes a lot more fun when you get to know a city intimately.

My family.

The heady mix of Englishness, colonialism, Pacific Rim polish, deeply steeped First Nations history and outsize wilderness can’t be found anywhere else on the continent.

Seeing coyotes in the city.

Anonymity.

[The Celebration of Light](http://youtu.be/gqmsd6OQndw).

Being a short train ride away from Portland, OR.

My cat loves rain, which seems to be a very Vancouver-cattish way to be.

78&deg; summers.

Being able to wear boots and layers for most of the year, except in July and August when you trade them in for Birkenstocks and floaty dresses.

Bicycles **belong** here.

I don’t need highways to get from one end of the city to the next.

Burrard Inlet.

Seal and dolphin sightings in Burrard Inlet.

Orcas.

Weird public art, like the laughing statues, the strange animal statues outside the Kensington library branch, the wedding rings at English Bay, and the 8-bit orca at the Vancouver Convention Centre.

Nobody cares if you smoke pot. Or if you don’t.

Because if I leave, the city’s black population drops by a fraction of a percentage point. That may not seem like much, but when the black population hovers around 1%, every one of us counts.

Being able to say "I'm Canadian" when I look and sound like I do is always a source of great joy.

Down deep I was always meant to be a West Coast girl. Vancouver allowed me to make that geography mean something more than mountains, ocean, rain, and sushi. The West Coast is (and hopefully always will be) **home**.
