---
layout: post
title: "Reader's Advisory Help Needed"
share: true
comments: true
modified:
categories: 
excerpt:
tags: []
image:
  feature: dork-diaries.jpg
date: 2014-10-26T18:38:47-07:00
---

I'm hoping some of you in library land can help me out. My sister is looking for book recommendations for my 11-year-old niece[^1] who is developing an interest in African American history. I've never been a children's librarian, nor have I done any reader's advisory, so I'm out of my depth. Here's what my sister had to say about her interests: 

> She likes books like the [Dork Diaries](http://dorkdiaries.com) or science fiction. She told me once that she found *The Diary of Anne Frank* interesting. Maybe some fiction with a little Black history mixed in. A few biographies written for kids her age.

The only titles that came to mind were the [Octavian Nothing](http://mt-anderson.com/blog/his-books/books-for-teens-and-adults/the-astonishing-life-of-octavian-nothing-part-1/) books, but they might skew a bit old for her. I'd also like to promote books that are about female characters and/or written by POC women. I adore [Jacquline Woodson](http://www.jacquelinewoodson.com/)'s books, but I don't know if any are historical fiction. Comments are on below, so feel free to add titles and links (if you have them). Thanks!

[^1]: She reads on an 8th grade level. 