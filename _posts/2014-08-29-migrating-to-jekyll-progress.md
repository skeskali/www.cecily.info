---
layout: post
title:  Migrating to Jekyll - A Progress Report
date:   2014-08-29
share:  true
comments: true
summary: Jekyll and I have a love/hate relationship.
---
## Migrating to Jekyll — A Progress Report

I'm only a budding developer, so my experience with getting [Jekyll](http://jekyllrb.com) up and running on Amazon S3 has been frustrating. I've learned a lot, but I've also spent a lot  of time reading documentation, searching for solutions, scrapping full installations and starting all over again. I thought I'd record the tasks/processes that worked in case it helps someone else.

Let me start by saying that there are as many ways to make this setup work as there are developers. If these instructions don't work for you, take to your search engine of choice, make a cup of tea, keep a notebook handy, and settle in for awhile.

## Setting Up Amazon S3

This was probably the easiest part of the process. I followed [Tiffany B. Brown’s instructions](http://tiffanybbrown.com/2014/08/19/moving-from-wordpress-to-a-static-blog/ "Moving from Wordpress to a Static S3-hosted Blog") for setting this up as they were the clearest and most concise. The key steps to remember are:

1. Your bucket name should be the same as your domain name. If you want your site to show up as `www.yoursite.com`, use that name for your bucket. If you want it to show up as `yoursite.com`, create the bucket with that name.
2. Configure your bucket for the index page and error page.
3. Create a bucket permission policy to allow read-only access to the files in this bucket. You can [use mine](https://gist.github.com/skeskali/3f7735e3c03f020d01a6 "S3 Bucket Permissions Policy"), just change `cecily.info` to `yoursite.com`
4. . If you want `www.yoursite.com` to redirect to `yoursite.com`, create a second bucket named with the `www` and configure this bucket to redirect to the yoursite.com bucket.

## Getting Files into S3
I’m on a Mac and Panic’s [Transmit](http://panic.com/transmit "Transmit FTP/SFTP/S3") is my flie transfer app of choice, so I used it to get my index.html and error.html files to my S3 bucket. [Cyberduck](http://cyberduck.io "Cyberduck") also works. You can also upload these files with S3’s browser-based bucket manager.

## Getting Started with Jekyll
Configuring Jekyll to run locally wasn’t too difficult.   Jekyll’s [Quick Start Guide](http://jekyllrb.com/docs/quickstart/ "Jekyll Quick-Start Guide") will walk you through the steps of getting a basic boilerplate site up and running[^1]. The steps to remember:

1. Install Jekyll
2. Run `jekyll serve` from your command line.
3. Open `http://localhost:4000` in your browser.

## Sync Jekyll with S3
We’ve arrived at my least favourite part of the process and the one that caused the most confusion.

Many of the instruction pages I read online said that all I needed to do to get my static Jekyll site running on S3 was to add the `_site` directory to my bucket. I thought I could just add the pages via Transmit or Cyberduck, but when I did that, my pages didn’t show up properly.

After many hours of searching, I ran across Paul Stamatiou’s [beautifully detailed instructions](http://paulstamatiou.com/hosting-on-amazon-s3-with-cloudfront/ "Hosting on Amazon S3 with Cloudfront - Paul Stamatiou") on using the `s3_website` gem to build your site and publish it to S3[^2]. The steps to remember:

1. Run `s3_website cfg create` in the root of your Jekyll directory. This will create the `s3_website.yml` configuration file.
2. Open the `s3_website.yml` file in your favourite editor and add your AWS access key, secret key, and bucket name to the file. Save the file.
3. Run `s3_website push` from the command line. This will build your site and push your site directory to S3. After this finishes, visit your Endpoint URL to see if your site is running.

## Total Time Spent
I estimate that I’ve spent approximately 10-15 hours over the last two days on this project, and that may be a conservative estimate. In comparison, I can deploy a simple WordPress blog in less than five minutes.

## Next Steps
Don’t get me wrong - I’ve really enjoyed learning how to get a static Jekyll website running on Amazon S3. But there are still quite a few things I have to sort out, such as: 

- Changing my DNS so that it points to my S3 bucket
- Figuring out how to reformat nearly 1,000 entries to Markdown
- Adding Jekyll FrontMatter to each page
- Importing those pages and writing redirects so that all of the images and relative links display properly on the static site

I’m wondering if the process is wortht the time and effort just so for the sake of bragging rights.  WordPress _works_ when I need it to (for the most part), and Dreamhost has been a reliable webhost in the 10+ years I’ve been a customer. Would I save money by moving to a static site on AWS? Sure. Would switching to Jekyll save me _time_? I am not convinced it would.

Still, there’s something to be said for learning new tech for the sake of learning. I may ultimately decide to leave cecily.info in WordPress and find some other use for my Jekyll static site.

Stay tuned.

[^1]:   This step assumes you’re comfortable with the command line. If you aren’t, you might want to start with [Github Pages](https://pages.github.com "Github Pages").

[^2]:   You don’t have to know your way around Github to make sense of this but it wouldn’t hurt. You do need to be comfortable with installing ruby gems from the command line, however.
