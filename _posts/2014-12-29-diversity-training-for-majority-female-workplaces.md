---
layout: post
title: "Diversity Training for Majority Female Workplaces"
comments: true
share: true
modified:
categories:
excerpt:
tags: []
image:
  feature:
date: 2014-12-29T06:54:21-08:00
---

While listening to [Episode 7](http://www.thebroadexperience.com/listen/2012/8/15/episode-7-non-white-and-female.html) of [The Broad Experience](http://www.thebroadexperience.com/), a podcast about women and workplace issues, the seed of an idea implanted itself in my brain.

I am thinking about creating a diversity workshop for employees who work in majority-female environments.

I frequently find it difficult to express myself properly at work. I'm told I speak out of turn, am "too confrontational," and my emails are read as terse. I'm generally thought of as unfriendly, which causes no end of stress and anxiety. And while I will cop to my shortcomings, I believe that a large part of this comes down to cultural misunderstanding.

My white colleagues can't possibly know what it's like to be a non-white person in this environment. They can't possibly understand how hard I work at being sufficiently deferential, at not speaking up about things that seem unjust, or that the heat of an ever-present klieg light of scrutiny becomes suffocating.

The workshop would cover some of these cultural differences, of course, but what else could/should it cover? Should there be one workshop for whites, and one that serves as a support system for non-white library workers?

When women come together in a majority, we sometimes think that power relations aren't at play. I'd like workshop participants to come away with an understanding of how power relations manifest themselves between women. The learning outcome should be that women acknowledge the negative impact of this unequal balance between WoC and white women in the workplace. They should be able to recognize the signs, and those women who are in positions of power would walk away with enough tools to work toward dismantling the imbalance.

At the same time, I'm wary of this becoming a session all about *fee-fees*, and I'm reluctant to be a part of anything that comes across as "Sassy WoC tell white women off and make them feel bad about themselves."

This feels very rudimentary and scattered, but it feels like there's *something* there. A very small something, but still something.

More to come.
