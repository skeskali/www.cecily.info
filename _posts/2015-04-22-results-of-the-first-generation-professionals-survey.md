---
layout: post
title: "Results of the First Generation Professionals Survey"
modified:
categories: 
excerpt:
tags: []
image:
  feature:
date: 2015-04-22T22:18:38-07:00
---

Here are a few preliminary results of the [First Generation Library Professionals survey](https://docs.google.com/forms/d/1q3FyctdeCdGP19hQBQzl4qvcLEztz_y4tYOB_LtKvH4/viewform): 

## Demographics

- Number of respondents: 36
- 19 academic librarians
- 7 public librarians
- 3 special librarians
- 16 new professionals
- 6 graduate students

## Preferred Ways to Connect (in order of preference from most to least popular)

- Regular twitter chats/hashtag
- Special interest group of professional library association
- Facebook group
- Google group/mailing list
- Slack channel

## Participation

- Most just want to participate
- Several people offered to be mentors 
- 10 people offered to help organize or to take on a leadership role 

## Comments

> It would be great if this could become a resource for people looking to enter MLS school as well - knowledge of things like graduate assistantships/how to negotiate funding would have been really helpful for me...

> Sometimes I feel very lost as a first-generation professional and having a support network is a really great idea!

> Neither of my parents are professionals, though I am not the first generation in my family to be a professional. There are lots of things that are hard for me to navigate, and I've only recently come to realize that this is one of the reasons why.

--- 

The survey will stay open indefinitely, but I think we're off to a good start. To those of you who said you'd like to help organize the network, I'll be in touch. 
