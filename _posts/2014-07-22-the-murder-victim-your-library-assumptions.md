---
layout: post   
title: The Murder Victim - Your Library Assumptions
date: 2014-07-22
share: true
summary: Say hello to the newest In the Library with the Lead Pipe editorial board member.
---
[I’ve just been appointed](http://www.inthelibrarywiththeleadpipe.org/editorial-board/cecily-walker/) to the editorial board of *[In the Library with the Lead Pipe](http://www.inthelibrarywiththeleadpipe.org/)*, a peer-reviewed journal founded by a team of international librarians who work in various types of libraries. Lead Pipe publishes works by authors from diverse perspectives, including librarians, support staff, administrators, technologists, and community members.

Initially I’ll mostly focus on editing and on working on the site redesign committee, but I’m also hoping my participation will provide a much-needed kick in the pants to get me to write more long form pieces.

This is a great honour, and I’m excited to work with such a [talented group of people](http://www.inthelibrarywiththeleadpipe.org/editorial-board/)!