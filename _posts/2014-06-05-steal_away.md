---
title: Steal Away
layout: post
share:  true
comments: true
summary: In which I daydream about getting away.
---
I dream of being kidnapped.

In this scenario, the usual suspects would show up at my front door, their faces serious but their eyes alight with secret mirth. &#8220;Renee, take Cecily to the car,&#8221; Kay would say, &#8220;I&#8217;ll take care of packing a bag for her.&#8221; Don&#8217;t worry about Ella, don&#8217;t worry about work, don&#8217;t worry about anything else except to tell me where you keep your iPad&#8217;s charger, because we are breaking you out.

There&#8217;d be two other carloads of people waiting downstairs, and much to my surprise, Tiffany, Michelle, and Erica would be there. How my BC friends pulled this off, I&#8217;ll never imagine. Renee would hustle me into the back seat of Kay&#8217;s SUV (Kay&#8217;s Jen always rides shotgun), and in the time it takes me to think of a reason why I shouldn&#8217;t go on this trip, we&#8217;d be on our way.

Mile after sun-drenched mile of highway would disappear under our wheels. We&#8217;d stop at roadside attractions (read that as roadside microbreweries) and sample the local brews. Phones would be passed around as we took funny pictures of each other, and both halves of my &#8220;logical family&#8221; would come together and get to know each other better.

After several hours of driving, we&#8217;d arrive in Penticton or Naramata, or someplace sunnier, much warmer, and more arid than Vancouver could ever be. &#8220;No, Cecily, don&#8217;t worry about it. Leave your stuff, we&#8217;ll bring it in. You take the downstairs bedroom and relax for a bit.&#8221; My friends would bustle around turning our rental into our weekend home. I&#8217;ll fuss and fret because I like to protest more than I like to help, and it&#8217;s important for me to put on a show of being broken up about not being able to scurry around.

The smell of applewood charcoal fills the air as Kay and Michelle tend to the grill. Tiffany&#8217;s over there mixing cocktails for everyone, and the way the sun glints off her movie-star sunglasses is a wondrous thing. Jen and Erica are talking about kickboxing and fitness training, and Renee is heading down to the riverside wearing a big floppy straw hat with an absurdly gaudy flower on it&#8217;s crown. I&#8217;m on the beach turning the colour of freshly-tilled soil, feeling my hair crackle and my nostrils dry out, but caring not one iota because being this warm, basking like this while surrounded by the people I love most is far more curative than any of the multi-syllabic medications I&#8217;m taking to treat my RA. Warm, wonderful women, laughter, drinks, food, and sunshine.

Who wouldn&#8217;t want to steal away for a weekend like that?
