---
title: A Collection of Library-related Thoughts
layout: post
date: 2012-09-11
comments: true
share: true
---

---
I’ve spent most of my work day sending email to move projects along…

### The Ethical Librarian
Lane Wilkinson muses on [ethical library service](http://senseandreference.wordpress.com/2012/09/11/on-ethical-reference-service/), or as I like to call it,”Why we can’t just give you the answer”:

> Basically, we teach at the reference desk because we have a librarian’s commitment to provide access to information conflicting with a professional commitment to honor our student’s external relationships. Teaching a student to look-up an article is, quite simply, just our way of circumscribing what we can’t do.

When I first started working in libraries, I had high minded ideas about teaching people how to use resources, but after awhile, there&#8217;s only so much eye-rolling a person can take before they come to the conclusion that giving someone the answer is what&#8217;s called for at times, and doesn&#8217;t mean we&#8217;re providing sub-par customer service. The challenge &#8212; and expertise &#8212; comes in knowing which service to provide and when. 

### That&#8217;s Mighty White of You&#8230;

NPR’s ombudsman wonders [When A Popular List Of 100 “Best-Ever” Teen Books Is The “Whitest Ever”](http://www.npr.org/blogs/ombudsman/2012/09/10/160768675/when-a-popular-list-of-100-best-ever-teen-books-is-the-whitest-ever) who’s to blame? 

> After speaking with editors and studying the poll, I find that the problem was not the experts, but the nature of the poll and the make-up of the audience. This is not to condemn either—let’s celebrate engagement!—but it does raise a question as to how NPR should protect its editorial integrity when publishing a popularity list that realistically will be taken as NPR’s own and have great influence in schools and sales.

My short and somewhat snarky answer? It’s a listicle[^1], people. _Relax_. 

My somewhat longer answer: Given the demographics of NPR’s audience, I’m not sure why we expected a different result. I don’t think that getting library and publishing professionals involved in vetting what is essentially a popularity contest is the answer. It smacks of a soft paternalism that I’m uncomfortable with. I do know that regardless of NPR’s stature as a media tastemaker, I don’t put any more weight behind their list of 100 Best Ever Books than I would behind Rolling Stone’s list of 100 Best Albums Ever (or Pitchfork — choose your generational signifier). Neither should you. This was a numbers game, and the makeup of the numbers — fairly or not — skewed the list in a particular direction. 

As professionals, from the time we enter library school to the time we accept our first entry-level job, we’re taught to make sure we consult a variety of sources when making collection decisions. NPR shouldn’t be the only arbiter of what is cool or “best”. If you’re genuinely concerned about this issue, and if you’re a librarian/teacher/bookseller who wants to expose your readers to more diverse works, **you can continue to do that**, even while being supportive of and promoting the NPR list. It’s not my intention to gloss over issues of diversity, but what I am saying is that library professionals continue to wield a significant amount of influence with our local communities. NPR’s list, no matter how good or how short-sighted, won’t take that away from us. 

### Riddles, Puzzles, Jerky Co-Workers, and Information Architecture

When I worked as a library assistant in the circulation department of my undergraduate library, I had a nasty habit of chaining together all the paperclips in our paperclip holder, but positioning them in such a way that they looked like individual paperclips. Not content to clip them end-to-end, I would double back and create really complex, helix-like strands of clips so that when you picked up the one paperclip on top, you would either pull out one long contiguous chain, or pull out something that looked like a metallic ball of twine. Today I used this as a metaphor to describe the information architecture on my library’s website. 

*File Under:* Eating my own dog food.

[^1]:   a portmanteau word, a conflation of the terms list and article. It is generally used in journalism and blogging to refer to short-form writing that uses a list as its thematic structure, but is fleshed out with sufficient verbiage to be published as an article.