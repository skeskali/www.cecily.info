---
title: Coder, Teach Thyself?
layout: post
comments: true
share: true
date: 2012-08-29
---
As someone who has struggled with developing my coding/programming skills past a certain point, [Andromeda Yelton][1]&#8216;s [post about coders needing to self-teach][2] (and the false dichotomy therein) feels like the right answer at the right time. G&#8217;wan. Read it.

 [1]: http://andromedayelton.com/about/
 [2]: http://andromedayelton.com/blog/2012/08/29/what-i-do-and-dont-believe-about-coders-need-to-self-teach/