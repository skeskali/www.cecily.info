---
title: Scene From the Bike Lane
layout: post
share: true
comments: true
---
It&#8217;s a gorgeous sunny day, and I&#8217;m pedalling up the Hornby bike lane. I&#8217;m on my way to get blood work done, but I&#8217;m feeling quite happy because the sky is a fantastic shade of blue, my knees don&#8217;t hurt, and the streets are teeming with people for me to gawk at. I&#8217;ve stopped at the light at Georgia/Hornby intersection, and a rangy white guy who looks to have seen better days makes eye contact as he crosses the street. A mile-wide smile spreads across his face as he draws nearer.

&#8220;Happy Black History Month!&#8221; he calls out loudly, pausing on the median to have a chat with me. Some pedestrians smirk at his exuberant greeting, some give us the side-eye, but mostly we&#8217;re ignored. I smile and thank him, but I tell him he&#8217;s a day early.

&#8220;Well, shit&#8221;, he chuckles, &#8220;I missed Martin Luther King Day completely, so I&#8217;m just happy I remembered at all!&#8221; We share a laugh and I raise my fist for a fist bump, which he heartily returns. The bike signal turns green, and I ride away, happier for this small moment of shared humanity.