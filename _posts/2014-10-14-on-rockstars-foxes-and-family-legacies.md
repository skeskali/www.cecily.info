---
layout: post
title: "On Rockstars, Foxes, and Family Legacies"
share: true
comments: true
excerpt:
tags: []
date: 2014-10-14T23:54:35-07:00
---

### A Foxy Librarian

A [very nice thing happened to me](http://gavialib.com/2014/10/ada-lovelace-day-cecily-walker/) today. The Library Loon wrote a profile about me in honor of Ada Lovelace Day:

> The Loon has not uncommonly pointed students to Ms. Walker’s writing and social-media presences. Any students who come to resemble Ms. Walker are impressive students indeed, and all the Loon’s students are fortunate that she is their example.

I'm not one to toot my own horn. I don't (think) I chase notoriety, though I do take a certain amount of pleasure in being a [sparklepony](http://cecily.info/2013/01/25/advice-from-a-sparklepony/). I don't think it would be wrong to say that my typical _modus operandi_ is keeping my head down and avoiding (professional) detection. This is why not only was I surprised to hear  about this profile, but it may explain why I felt it so deeply. Without going into detail, I will simply say that this profile came exactly when I needed to be reminded that the work I do outside of my library has a great deal of value; perhaps even more value than the work I do within the library's walls.

### On Rockstars
The concept of rockstar librarians seems to be on the radar (again). If you're going to read anything about it, read [Coral's piece](http://www.sheldon-hess.org/coral/2014/09/rockstar-librarians/), because she makes the point that it's irresponsible to wield power unwisely. Not only that, it's disingenuous to deny that your status conveys power, but that's another topic for another time.  

The idea of rockstar librarians got me thinking. What qualities come to mind when you think of a rockstar?

* Talent
* Notoriety
* Hard work
* Practice, practice, practice
* Awards and accolades

You know what else comes to mind when I think of rockstars?

* Sycophants
* Distance
* Bodyguards that are hired to keep rockstars away from the fans
* Groupies
* Excess
* Debauchery (hi, library conferences)
* Hubris

Also, if you aren't selling out arenas, I don't think you can legitimately call yourself a rockstar, no matter how many keynotes you're invited to give at library conferences each year.

This isn't envy or jealousy talking. I think people -- especially women --  should market themselves and be proud of their accomplishments. It's just that the term comes with so much baggage, and it's baggage that seems at odds with the stated mission and goals of the profession.

### Family Legacy
Henry Louis Gates' Finding Your Roots airs on PBS Tuesday nights at 8pm. Every time I see an episode, I become more and more convinced that I should quit my job to become a genealogical researcher. I need the steady paycheques so I won't be quitting anytime soon. Instead I spent a few hours researching my family's history. I've hit a brick wall with information from my mother's side of the family, but I made a major discovery on my father's side.

![Screenshot of Census Record from 1900](http://cecily.info/uploads/2014/10/greatgrands.png)  

This screen capture is from the 1900 US Census. The names you see here -- Amous and Florence Walker -- were my paternal great-grandparents. Amous and Florence were born in Georgia in 1864. This means that __Amous and Florence were the first free people in my family__. I don't have the words to adequately describe how this made me feel, but I will say that it affirmed my place in history in a way that a thousand documentaries about slavery or the civil rights movement never have.  
