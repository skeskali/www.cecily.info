---
title: 'Scenes From a Commute &#8211; A Boy and His Bunny'
layout: post
share: true
comments: true
---
I don’t exactly recall how this exchange between strangers turned into one of my favourite things about my daily ride through my neighbourhood< but I’ve lived long enough to know that life will always find ways to surprise me.

Because I don’t cook much these days (standing for longer than 5 minutes at a time is too difficult), I spend a lot of time going to the faux-fancy market down the street from my apartment for ready-made meals. I usually stop on the way home from work, park Rizzo out front, and hobble my way inside without so much as a second glance at other people on the street. But when someone is sitting in front of the store with a tiny tan and white rabbit,  I can&#8217;t help but notice.

The rabbit’s owner is a youngish man, probably in his early 30s, and is in a wheelchair. He keeps his hair buzzed short, and the most brilliantly blue eyes I’ve ever seen. They’re the kind of blue that can’t be disguised by nightfall. Yet as bright as they are, some days they seem a little vacant. I say that without judgement, because I have no idea what he has to do to make it through his day, but it is something I notice whenever I see him.

I wanted to know more about this man, and about how he and his tiny little friend found each other, but I was shy and he was wary. It wasn’t until the third time we saw each other in front of the store that we finally exchanged words.

“Hey, you weren’t limping like that when I saw you the other day,” he said as I awkwardly dismounted from my bike. His voice was friendly, but concerned. I didn’t want to give him my life story, but I explained how I get these shots in my bad knee and for a few days afterward, walking is really difficult. He gestured to his legs. “I know,” he said, with a heartbreaking amount of tenderness in his voice. I felt like an oblivious and privileged jerk.

I asked him if he needed anything from the store, not because I’m hesitant to give panhandlers money, but to make this encounter a little more personal. I thought he’d ask for something to eat or something for his pet, but instead he asked for lemonade drink crystals, the kind of sugar-laden goodness that I haven’t been able to enjoy for years.

On a different night he saw me leaving the drugstore. “You’re walking pretty good today!” he said as I walked to my bike. I was on the way to meet friends and couldn’t stop to chat, but as he saw me riding away he called out “You’re riding pretty good, too!” I rang my bell as a salute and pedalled away with a wave.

On Thursday, at the end of an emotionally and physically demanding day, I saw him in front of the grocery store. Part of me was happy to see him. He had some track lighting in his lap, and his little brown buddy was sitting at his feet, calmly sniffing the sidewalk. I was having a hard time finding a place to lock my bike when he wheeled over and parked his chair at the end of the bike rack. “Hey, why don’t you just leave it here? I’ll watch it for you.”

And even though I eventually found a place to lock up, for the briefest moment I trusted him enough to seriously consider his offer. I can’t explain why, but I did.

He said, “You’re limping pretty bad today,” and this time I could clearly hear the concern in his voice. I tried to downplay it, to divert attention away from it, but he wouldn’t let me. “I know what that’s like, to enjoy something that seems so easy, but then you have days when you just can’t make things work. One day I was jumping my bike off stairs and trails, and the next day I’d lost everything.”

The silence was awkward and pregnant, and all I could do was sheepishly agree with him. For some reason I felt ashamed, and I most certainly felt exposed, seeing that the mask that I wear every day had been so artfully and carefully stripped from my face by someone who didn’t really know me at all.

I asked him about the lights, and he told me some guys from the construction sites across the street gave them to him. “I’m going to take it home, put them over a little sod that they gave me, and plant her a little garden in my apartment. I know a little something about lighting. I used to work construction before.” He didn’t have to say before when, because we both knew when he meant. Before his accident, he had been a construction foreman. He’d also been something of a daredevil on a bicycle.

“Yeah, a couple of days before I had my accident over on the north shore, I jumped my bike off the roof of the convention centre onto the bike path below. It was CRAZY!” he said, and his eyes lit up and his face was more animated than I’d ever seen it.

I offered to buy him something from the store, and he accepted, and asked me if I wouldn’t mind “throwing in some parsley for her.” After I made my purchases and handed him a bag with a sandwich and some parsley in it, I stuck out my hand and said “I’m Cecily. What’s your name?”

“I’m Theo. Nice to meet you Cecily,” and he tenderly shook my hand. As I worked at untangling my bike from the pretzel of cables, handlebars and locks, I heard him mutter my name. “That’s a nice name. What’s that from?” I tried explaining the Cicely Tyson/Cecily thing to him, but he hadn’t heard of Ms. Tyson before, and that’s when I realized just how young he really was. “I’ll try to remember it but I might not always get it right. Don’t get mad at me, OK?” I promised him I wouldn’t, and said he could just call me “C” if it was easier.

We talked a bit more about bikes, about pets (the rabbit’s name is Babies), and then I turned to pedal home. As I rode away, he called out &#8220;Keep going, you&#8217;re almost home!&#8221;

I don’t know whether getting so familiar with a panhandler is a good or a bad thing, and I’m not really sure I care. Instead, I’d rather work from a position of trust and openness rather than suspicion. I may come to regret it at some point; I sincerely hope I don’t. But until then, I’m going to enjoy getting to know Theo and Babies a little better as I’m sure to run into them again.

Besides, I want to hear more about Babies’ garden patch.