---
layout: post
title: "Migrating to Jekyll - Part 2"
share: true
comments: true
modified:
categories: 
excerpt:
tags: []
image:
  feature:
date: 2014-09-07T11:21:55-07:00
---

DNS finally propagated, I found a theme I can live with that doesn't throw up XML-parsing errors when implemented on Amazon S3 (WTF [Lanyon](https://github.com/poole/lanyon)) and one with some nice features like search and Disqus integration built-in. The migration from WordPress to Jekyll is complete. Getting to this point feels like a quite an accomplishment. 

I'll grant that choosing a theme instead of building my own removed the largest barrier to this project.  When it comes to working with code, I retain information better when I start with a finished product and try to piece together how the developer makes it work. 

When I  take a finished product and customize it to suit my needs, I spend less time paralyzed with fear and indecision over whether I'm getting things *exactly right*. I can make as many small tweaks as I like while I suss out how things hang together. The [archive page](/archive/) I built for the site is an example of my learning process. I knew I wanted an entry archive on the site, but the So Simple Theme doesn't include one. I looked around and found Julie Mao's [archive page](https://gist.github.com/jules27/3b8f0f25da4ca4e5ffae#file-archive-html) and used it as the template for my own. 

Although the migration is complete, the learning never stops. The next issue I need to resolve is how to construct a workflow gives me the freedom to publish (and tinker) from any computer. Publishing with Github Pages instead of Amazon S3 would be easier; I'd clone the site on each computer I have access to, fire up a text editor, and with a simple ```git push``` my changes would be live. Right now I think using Dropbox to store my posts and templates and running Jekyll and [s3_website](https://github.com/laurilehmijoki/s3_website) to build and deploy the site might be the answer I'm looking for. 

My greatest hope for this project is that I develop enough confidence as a developer and as a writer to produce more, to talk more about my challenges and successes, and to share with and learn from other people. I'm eager to explore the social side of writing code; the possibility of meeting new people excites me almost as much as learning new skills. 