---
title: Manually Adding Kobo's Adobe Digital Editions DRM Files to Your iOS Device
layout: post
share: true
date: 2012-04-29
---

**NOTE**:  This post was originally posted on **29 April 2012**. The content may be outdated.

Recently I ran into a small problem where all of the titles I purchased through Kobo, a Canadian-based e-book retailer, disappeared from my iPad and iPhone. The titles still appeared on my Kobo Touch reader, and oddly enough, I traced the disappearance to about the same time that I synced all my titles to the ereader.

After searching around for help and not finding much, I came up with the following fix.  

* First, log into your Kobo account and download the Adobe DRM EPUB file to your computer.

<img src="/uploads/2012/04/kobo1.png" alt="" title="kobo" width="400" height="294" class="frame aligncenter size-full wp-image-3886" />

* Next, locate your ACSM files in your designated downloads folder. The files will launch Adobe Digital Editions, and your titles will appear in the ADE reader.

<img src="/uploads/2012/04/ade.png" alt="" title="adobe digital editions" width="400" height="259" class="frame aligncenter size-full wp-image-3888" />

* Find the Digital Editions folder on your Mac&#8217;s hard drive. Chances are it&#8217;ll be in *~/Documents/Digital Editions*. Once there, you&#8217;ll see the EPUB files for your books.

<img src="/uploads/2012/04/finder-1.png" alt="Files in Finder" title="finder-1" width="400" height="255" class="frame aligncenter size-full wp-image-3895" />

*  Connect your iOS device to iTunes. Select the device in the Devices list, and then click Apps. From there, look for the Kobo app under File Sharing. Click the app to make it active, and then, drag and drop the EPUB files from your hard drive to the Kobo app in iTunes.

<img src=/uploads/2012/04/iTunes.png" alt="iTunes" title="iTunes" width="400" height="231" class="frame aligncenter size-full wp-image-3890" />

* Sync your device, reopen the Kobo app, and voila, your titles are on your iOS device.

If there&#8217;s an easier fix to this, I haven&#8217;t yet found it. I&#8217;ve also not heard anything from Kobo yet on why this has happened, and why I can&#8217;t simply sync my library to my iOS devices and have the titles re-appear. This is a kludgy workaround that, for an iOS user, reminds me more of the experience library patrons have with downloading DRM-protected titles from library e-book collections, rather than the seamless experience I&#8217;ve come to expect with purchasing items through my iOS devices.
