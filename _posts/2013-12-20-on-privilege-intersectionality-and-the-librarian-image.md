---
title: On Privilege, Intersectionality and the Librarian Image
layout: post
share: true
comments: true
---
Thanks to some conversations I&#8217;ve had with <a href="https://exitpursuedbyabear.net" title="Exit, Pursued By a Bear - Lisa Rabey" target="_blank">Lisa Rabey</a>, <a href="http://andromedayelton.com" title="Andromeda Yelton" target="_blank">Andromeda Yelton</a>, and the members of #libtechwomen, I&#8217;ve been thinking about intersectionality in librarianship a lot lately. A b<a href="http://agnosticmaybe.wordpress.com/2013/12/20/a-libraryland-festivus/" title="A Libraryland Festivus" target="_blank">log post by Andy Woodworth</a> provided just the jumping off point I needed to publish some of these thoughts. 

While [airing grievances][1], Andy touched on the idea of professionalism in librarianship, and how, in his mind, it is tied to a person&#8217;s appearance. He wrote: 

> (H)ow you look and act around the community you serve *matters*. How you dress is up to you, but if you step outside of the people’s expectations as to how [insert your kind of librarian] should look it’s going to take work to show them that you are a competent professional. It’s not up to them to expand their definitions, it’s up to you to do the work that will prove those definitions are wrong.

I take issue with this perspective, because I think it perpetuates inequality in librarianship, and it privileges a specific group. I&#8217;ll try to explain why. 

The demographics of this profession fall largely along white, cisgender female lines. So much so, that if you search for images of librarians, white cisgender women will make up the vast majority of your search results. 

Anyone who falls outside the white cisgender female model defies expectations of what it means to be a librarian by simply existing. Even male librarians represent an expansion of the definition. 

In my case, you see evidence of this expectation when patrons approach a white colleague at the reference desk instead of coming to me for help because I don&#8217;t fit their model of what a librarian looks like. I&#8217;ve been told this also happens to male librarians. 

Regardless of what I wear or how I act around some members of the community I serve, my race will always place me outside of the norm. When we place the burden of of being the exception on those who fall outside of the norm, we are furthering an agenda that supports the idea that whiteness is the highest standard, indeed, the **only** standard that should be used to measure suitability. 

Rather, I think the responsibility lies with the community as a whole to demonstrate that differences (race, gender, sexual orientation, able-bodied, etc.) aren&#8217;t just deviations, but are representative of much stronger, deeply entrenched power relations that must be challenged and dismantled if this profession hopes to diversify. 

We can have conversations about purple hair and tattoos and whether they don&#8217;t represent a professional image, but we shouldn&#8217;t have them without drawing parallels between these superficial differences and the (in some cases) immutable differences that we are born with, or that are central to our identity. 

This is what intersectionality means. The intersection between and within these differences presents us with an opportunity to work toward a more beneficial and productive dismantling of the structures that work to keep us apart and distrustful of each other. 

I am a woman who works in library technology (to an extent), so I am very interested in discussions, conferences, and panels on gender in library technology. However, by focusing the discussion only on gender, it&#8217;s as if I&#8217;m being asked to disassociate the other parts of my identity (i.e., race) because it isn&#8217;t viewed as a universally shared experience. It reinforces the privilege of white cisgender women, and when we support this privilege, we fail to appreciate how these implications for my professional success and standing intersect and interact. 

But I don&#8217;t want to give the impression that discussions about intersectionality are only about the ways we continue to fail each other. As <a href="http://theangryblackwoman.com/2009/08/02/intersectionality/" title="Intersectionality - The Angry Black Woman" target="_blank">The Angry Black Woman writes</a>, intersectionality is &#8220;also about taking in on yourself to learn, to form better bonds, to understand, to change yourself the way you’ve asked others to change.&#8221;

That&#8217;s what I&#8217;m hoping to do with this post, to create a space where we can discuss these concepts inside (and outside) of professional contexts with an eye toward dismantling those structures that work to keep our profession from diversifying. 

(h/t to <a href="http://chrisbourg.wordpress.com" title="The Feral Librarian" target="_blank">Chris Bourg</a> for letting me <a href="https://twitter.com/mchris4duke/status/414111837987160064" title="Chris Bourg - Twitter" target="_blank">ride her coattails</a>.)

 [1]: http://www.youtube.com/watch?v=xoirV6BbjOg "I got a lotta problems with you people!"