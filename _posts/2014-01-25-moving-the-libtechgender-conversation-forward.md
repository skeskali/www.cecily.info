---
title: Moving the Libtechgender Conversation Forward
layout: post
share: true
comments: true
---
I&#8217;ll flesh these out later, but I wanted to quickly share some notes I made this morning as I prepared for the Challenges of Gender Issues in Library Technology panel at ALA Midwinter. Maybe they&#8217;ll help others to understand where I&#8217;m coming from. 

EDITED TO ADD: a credit to Race Forward and their paper &#8220;<a href="http://www.raceforward.org/research/reports/moving-race-conversation-forward" title="Moving the Race Conversation Forward" target="_blank">Moving the Race Conversation Forward</a>&#8221; and <a href="http://www.youtube.com/watch?v=LjGQaz1u3V4" title="Moving the Race Conversation Forward (YouTube)" target="_blank">Jay Smooth</a> for articulating these concepts in such an accessible way. 

*   We need to move the conversation forward and talk about not only internalized and interpersonal failings, but systemic barriers to equality and advancement.
*   We tend to focus on individuals instead of systems. The ALA CoC covers interpersonal actions. It’s important to address these intentional actions when they arise, but they’re the simplest to focus on and easiest to solve.
*   What are we doing on an institutional level to dismantle discriminatory practices that promote inequality in library technology?
*   How do we move this conversation forward?
*   Expand our understanding of sexism beyond personal prejudice and actions to systemic barriers
*   Focus on actions and impacts rather than attitudes and intentions &#8211; the ALA CoC does this to a degree
*   View through an intersectional lens; add conversations about race, class, sexual orientation, gender presentation when talking about gender in library technology
*   Examine the unfair policies that occur within your institutions that produce inequitable outcomes for people on the margins.

The problem of a panel that focuses solely on gender means that by focusing only on the most privileged group &#8211; white women &#8211; those who are “multiply-burdened” (Crenshaw &#8211; <a href="http://socialdifference.columbia.edu/files/socialdiff/projects/Article__Mapping_the_Margins_by_Kimblere_Crenshaw.pdf" title="Mapping the Margins - Kimberle Crenshaw" target="_blank">PDF</a>) are marginalized.

For white women, discussions about the role of gender in library technology gives the impression that but for their gender, they would not be disadvantaged. Their race is never a factor in their discrimination, and therefore it becomes the standard sex discrimination claim.

Things are more complex for women of colour or for lesbian, bisexual, or transgender women, because we can receive protection only to the extent that their experiences are similar to those whose experiences tend to be reflected in anti-discrimination rhetoric.