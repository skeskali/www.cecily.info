---
title: Chronic Illness and the Self
layout: post
share: true
comments: true
---
[<img src="/uploads/2013/11/injection-500x375.jpg" alt="methotrexate injection" width="500" height="375" class="aligncenter size-large wp-image-4760" />]

When you are some degree of unwell every day like I am, it becomes really difficult to determine when you should really take a day off, and when you should suck it up and go into work anyway. 

I&#8217;m in the middle of a rather large, strategically important project at the library. The project is going fairly well, and I&#8217;m proud of the progress my team has made, how my direct reports are starting to become more confident in their own abilities and decision-making, and I&#8217;m proud of myself for keeping a relatively level head and not flying off the handle (publicly) during frustrating moments. 

But this project isn&#8217;t only important to the library, it&#8217;s important to **me**, personally and professionally. My performance shows that I can rise to the challenge. Managing the project has given me the confidence boost I needed, and the strategic and theoretical underpinnings of the project helped solidify a number of deep-seated beliefs I have about service through technology. I&#8217;m under a white-hot spotlight at work as a result; I don&#8217;t think it&#8217;s an exaggeration to say that the success of this project could affect my trajectory at the library. Yes, it&#8217;s **that** big. 

Because this project is so significant, I&#8217;ve found it hard to step away when my body demands it. I downplay my RA[^1] symptoms because I don&#8217;t want pity, but the days that I phone in sick are days when I literally can&#8217;t walk more than the distance between my bed and the bathroom, or days when I can&#8217;t shower or dress myself because the pain in my arms and hands is too great. Even then I make the effort to check email, communicate with the project team, my manager, and the vendor via email, phone or chat. My manager once told one of the library directors &#8220;That&#8217;s the thing about Cecily; if she says it&#8217;ll get done, it&#8217;ll get done no matter what.&#8221; I felt proud in that moment, but maybe I shouldn&#8217;t have. Maybe it was a caution? A moderated expression of concern from a colleague? 

I&#8217;m writing this because I&#8217;m having a very rough day today. My calendar is blissfully empty except for one quick status meeting first thing in the morning, and I know this would be the perfect day to step back, rest, slow down the adrenaline that&#8217;s surging through my body, and just rest. My attendance record is pitted and pockmarked with numerous absences. I&#8217;m in danger of running out of sick leave. But my fear is that these absences, these days when my body forces me to press pause will impact my career aspirations. I&#8217;m scared that I&#8217;ll get a reputation for not showing up, not being able to do the work, not being available. 

Being constantly fatigued and in pain demands a lot of energy. Some days I can draw on my reserves and make it through the days, but today isn&#8217;t one of those days. Yet here I am wasting precious cycles by worrying that my position is slipping, that I&#8217;m letting my team, my supervisor, and the entire library down if I don&#8217;t at least show up every day. 

My workplace has shown me tremendous support and kindness throughout this illness. I only wish I could do the same for myself. 

Related: see Colleen Harris-Keith&#8217;s excellent post on <a href="http://guardienne.blogspot.ca/2013/07/the-limping-librarian-post-on-chronic.html" title="Guardienne of the Tomes - Chronic Illness in the Workplace" target="_blank">chronic illness in the workplace</a>.

 

 [^1]: rheumatoid arthritis