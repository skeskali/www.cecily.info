---
title: 'Spice Up Your Life: A Diverse List of Library/Tech Speakers'
layout: post
share: true
comments: true
---
My good friend <a href="http://tiffanybbrown.com/" title="Tiffany B. Brown" target="_blank">Tiffany B. Brown</a> had this to say earlier this morning:

<blockquote class="twitter-tweet">
  <p>
    Btw, takeaway for that <a href="https://twitter.com/tristanwalker">@tristanwalker</a> interview is also (roughly) &#8220;Get some black friends.&#8221;
  </p>
  
  <p>
    — Tiffany B. Brown (@webinista) <a href="https://twitter.com/webinista/statuses/400694967078633472">November 13, 2013</a>
  </p>
</blockquote>

&nbsp;

This tweet and the Tristan Walker <a title="Meet The Entrepreneur Who Wants to Change Silicon Valley" href="http://www.npr.org/blogs/codeswitch/2013/11/07/243783500/meet-the-entrepreneur-who-wants-to-change-silicon-valley" target="_blank">interview</a> in question were in my mind when a friend and colleague asked me to put together a dream speakers list that the library&#8217;s Policy & Planning group could use for possible speakers. This list is by no means exhaustive, and it was pretty much just made up of people I know personally and/or people I admire and have seen present at other events. 

If you&#8217;re a friend of mine and you&#8217;re not on this list, it&#8217;s probably because I know you don&#8217;t like/aren&#8217;t jazzed about public speaking, not because I don&#8217;t think you&#8217;re any good. 

Here&#8217;s the list:

*   <a title="Anil Dash" href="http://dashes.com/anil/" target="_blank">Anil Dash</a>
*   <a title="danah boyd" href="http://www.danah.org/" target="_blank">danah boyd</a>
*   <a title="Marshall Kirkpatrick" href="http://marshallk.com/" target="_blank">Marshall Kirkpatrick</a>
*   <a title="Gina Trapani" href="http://ginatrapani.org/" target="_blank">Gina Trapani</a>
*   <a title="Baratunde Thurston" href="http://baratunde.com/" target="_blank">Baratunde Thurston</a>
*   <a title="Steve Fisher" href="http://www.republicofquality.com/steve-fisher" target="_blank">Steve Fisher</a>
*   <a title="Shannon Fisher" href="http://www.republicofquality.com/shannon-fisher" target="_blank">Shannon Fisher</a>
*   <a title="Denise Jacobs" href="http://denisejacobs.com/" target="_blank">Denise Jacobs</a>
*   <a title="Michelle Jones" href="http://michellejones.net/" target="_blank">Michelle Jones</a>
*   <a title="Ernie Hsiung" href="http://www.erniehsiung.com/" target="_blank">Ernie Hsiung</a>
*   <a title="Erica Mauter" href="http://www.swirlspice.com/" target="_blank">Erica Mauter</a>
*   <a title="Trevor Dawes" href="http://trevordawes.com/" target="_blank">Trevor A. Dawes</a>
*   <a title="Courtney Young" href="http://courtneyyoung.org/" target="_blank">Courtney Young</a>
*   <a title="Andromeda Yelton" href="http://andromedayelton.com/" target="_blank">Andromeda Yelton</a>
*   <a title="Lisa M. Rabey" href="https://lisa.rabey.net/" target="_blank">Lisa M. Rabey</a>
*   <a title="Bohyun Kim" href="http://bohyunkim.net/" target="_blank">Bohyun Kim</a>
*   <a title="Andy Woodworth" href="http://agnosticmaybe.wordpress.com" target="_blank">Andy Woodworth</a>
*   <a title="Carson Block" href="http://www.carsonblock.com/" target="_blank">Carson Block</a>
*   <a title="Amanda Etches" href="http://weareinflux.com/about" target="_blank">Amanda Etches</a>
*   <a title="Aaron Schmidt" href="http://weareinflux.com/about" target="_blank">Aaron Schmidt</a>
*   <a title="Anna Creech" href="http://eclecticlibrarian.net/blog/" target="_blank">Anna Creech</a>
*   <a title="Emily Clasper" href="http://www.linkedin.com/in/eclasper" target="_blank">Emily Clasper</a>
*   <a title="Jessamyn West" href="http://librarian.net" target="_blank">Jessamyn West</a>

The demographic breakdown:

*   23 names
*   8 <a title="visible minority - wikipedia" href="http://en.wikipedia.org/wiki/Visible_minority" target="_blank">visible minorities</a>
*   14 women
*   8 men
*   3 Canadians

If I were to revise the list (and I may), I&#8217;d try to incorporate more Canadian voices/perspectives on this list, because library, privacy, and intellectual freedom issues differ so greatly between the United States and Canada.

Tristan Walker said  &#8220;If you&#8217;re not including what will be the majority demographic in our country at the table in positions of leadership, your company just could not be destined for the level of success it should be destined for.&#8221;  As someone from a woefully underrepresented demographic in my profession, I&#8217;m always thinking about ways to make library events more inclusive. I think some fantastic, creative thinking about technology and how it affects our lives happens within our profession, but I also think that some of the best thinking happens outside of libraries. The division between corporate culture and libraries, while still great, isn&#8217;t as much of a contentious point as it used to be; however, I made a conscious choice to include technologists who espouse a more human approach to the role and reach of technology into our personal lives.

Tiffany might&#8217;ve said &#8220;get some black friends,&#8221; but knowing her as I do, I&#8217;m sure she&#8217;d agree with me if I expanded that to &#8220;Get some black, brown, Asian, queer, international friends, too.&#8221;