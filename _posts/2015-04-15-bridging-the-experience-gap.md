---
layout: post
title: "Thoughts on Bridging the Experience Gap"
share: true
comments: true
modified:
categories:
excerpt:
tags: []
image:
  feature:
date: 2015-04-11T18:36:54-07:00
---

**Update** Let's make the first generation library professionals group a reality. If you have five minutes, [please take the survey](https://docs.google.com/forms/d/1q3FyctdeCdGP19hQBQzl4qvcLEztz_y4tYOB_LtKvH4/viewform). 

Before you read any further, take five minutes to watch [Ivy League Trailblazers](http://www.nytimes.com/2015/04/12/education/edlife/first-generation-students-unite.html?_r=0) at the New York Times. It'll provide a bit of background for what I'll be discussing in this post.

How do you learn to learn, to know what you don't know? As a first generation college student, the first in my family to attend integrated schools, and the first in my family to earn a graduate degree, I've had experiences that my parents and many of my siblings couldn't conceive of. Thanks to my background and lack of preparation, I've dealt with sometimes crippling amounts of self-doubt and shame in these environments, especially when faced with academic or professional challenges.

My parents weren't professionals. They always worked, but if you had to call them anything, "working poor" would be the closest fit. The stress of raising nine children in reduced circumstances took its toll on my parents, so they weren't always responsive when I had questions they couldn't answer. To this day, I find it hard to approach superiors -- or anyone, really -- when I need help. They taught me so many other skills -- resilience, resourcefulness, and optimism -- so the lessons I learned from them will continue to be a great source of pride. It wasn't until I started moving in more educated circles that I realized what I was missing. 

<a href="https://www.flickr.com/photos/richardpluck/216793672" title="&quot;First Generation&quot; by Chong Fah Cheong by Richard Pluck, on Flickr"><img src="https://farm1.staticflickr.com/77/216793672_91b76d8cc8_z.jpg?zz=1" width="640" height="629" alt="&quot;First Generation&quot; by Chong Fah Cheong"></a>

When you don't have a trusted peer network to depend on for advice, where do you turn? First generation ivy league students banded together to create a support network and conference that provides services, camaraderie, and strategies for successfully navigating the challenges that arise when you're out of your element. I'm inspired by this sort of grass-roots problem solving, particularly when it is provided in a judgment-free environment where everyone is committed to helping you succeed.

I wonder whether a similar network might be needed for [first-generation professionals](http://karenhinds.com/8-essential-skills-for-first-generation-professionals/) or first-generation graduate students? As a mostly middle-class profession, librarianship fails to address the cultural expectations that go along with membership in this community, such as cultural literacy, notions of professionalism that are rooted in the dominant culture, or access to wealth.[^1] Maybe the conversation could start as a special interest group of a professional library association. Then, provided the interest still exists, the group could propose a conference panel to address these issues at the organizational and individual level. 

--- 

The other day, a librarian I "internet know" asked why books that focused on teaching librarians to code or about marketing were needed when great books by marketers already exist. I wish I could understand the perspective that leads someone to ask a question like this, but my position on the margins makes that difficult.  

Coding groups for librarians, or trans people, or black women and girls exist because affinity groups are a key component of success. Anything that validates the experience of marginalized people, that makes us feel not only welcome, but  fundamental to a community's success is a **good** thing. Diversifying our voices and practices are **good** things, are **necessary** things if we want to do more than present diversity as a problem to be solved. 

[^1]: Such as access to education, the ability to take on and repay student loan debt, or the social currency that comes from being a member of the class that sets the standard.

