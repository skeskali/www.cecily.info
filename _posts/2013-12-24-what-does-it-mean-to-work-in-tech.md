---
title: What Does It Mean to Work &#8220;In Tech&#8221;?
layout: post
share: true
comments: true
---
Lately it seems most of the commentary that stimulates my thinking happens on Twitter. Or maybe it&#8217;s because that&#8217;s where I spend most of my time. At any rate, a couple of tweets got me thinking about what it means to work &#8220;in tech.&#8221; 

<blockquote class="twitter-tweet" lang="en">
  <p>
    Black media used the term "techie" too loosely. Every Black person who uses a computer and went to SXSW is not a "techie".
  </p>
  
  <p>
    &mdash; April Davis (@April_Davis) <a href="https://twitter.com/April_Davis/statuses/312640373258330112">March 15, 2013</a>
  </p>
</blockquote>



<blockquote class="twitter-tweet" data-partner="tweetdeck">
  <p>
    It’s becoming increasingly irritating when “women in tech” is applied, it means “women who code,” which is bothersome. Quite bothersome.
  </p>
  
  <p>
    &mdash; Ho. Ho. Ho. (@pnkrcklibrarian) <a href="https://twitter.com/pnkrcklibrarian/statuses/415588865479409664">December 24, 2013</a>
  </p>
</blockquote>



I&#8217;m a SXSW veteran, although I haven&#8217;t been back in about five years. While I wasn&#8217;t ever on a Blacks in Tech panel, I don&#8217;t think it&#8217;s a stretch for me to say that I was there when the panels were getting off the ground. But I wasn&#8217;t a developer, I only puttered around with code in my free time, and my work was largely in the user experience/usability/information architecture sphere. 

I went to library school because I was interested in the organization of information in online spaces (and because it was cheaper than biz school). I worked as a graduate student assistant in the department&#8217;s IT lab, where I ghosted computers in between every term, set up space on the LAN for students, and provided hardware and software support to my colleagues. Before that, I&#8217;d worked in technical support at an internet service provider where I helped people troubleshoot their internet, and later, cable modem connections. I moved into the web hosting department, where having to set up FreeBSD machine from scratch (including the desktop environment of your choice) was a condition of employment. 

I&#8217;ve written shell scripts, and I&#8217;ve been the equivalent of a junior server administrator. These days, I&#8217;m an assistant manager in the digital services department at a major urban library. But would you say that I work &#8220;in tech&#8221;?

Discussions about core technical competencies for librarians and library technical staff are *de rigueur* these days, and as often happens in many of these discussions, someone makes the claim that it is absolutely necessary that librarians learn to code, because being able to build our own stuff is the future (I guess). 

But is it, really? 

What if, instead of asking people to only focus on the hard skills of coding/being a maker, we asked people to learn more about ways they can use technology to their advantage? What if we didn&#8217;t only privilege production, but we made room for mediation, collaboration, and interpretation? What if we decided that knowing how can we use this technology in the service of our patrons and other staff is just as important a skill as knowing how to using a vendor&#8217;s API to build a web app that is tailored for your specific library&#8217;s needs? 

Knowing how to write &#8212; and interpret &#8212; code is a solid marketable skill. But so is knowing how to talk tech. Splitting hairs on whether someone who knows how to use a laptop is a real techie is an artificial distinction, and y&#8217;all know how I feel about [distinctions that keep us apart][1]. 

Future-ready librarians won&#8217;t (only) know how to code, they&#8217;ll know how to speak the language and adapt it in such a way that everyone in their audience &#8211; from the most experienced, to the least &#8211; will be able to easily understand technology and it&#8217;s uses. 

Additional reading that inspired this post: &#8220;<a href="http://99u.com/articles/20696/you-dont-need-to-learn-to-code-other-truths-about-the-future-of-careers" title="You Don't Need to Learn to Code" target="_blank">You Don&#8217;t Need To Learn To Code & Other Truths About the Future of Careers</a>&#8221; by <a href="https://twitter.com/seanblanda" title="Sean Blanda - Twitter" target="_blank">Sean Blanda</a>.

 [1]: http://cecily.info/2013/12/20/on-privilege-intersectionality-and-the-librarian-image/ "On Privilege, Intersectionality, and the Librarian Image"