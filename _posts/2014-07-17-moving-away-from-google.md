---
layout: post
title: Moving Away From Google 
date:  2014-07-17
comments: true
share: true
summary: In which I bid farewell to the data behemoth.
---
Lately I’ve been thinking about the degree to which I’m willing to turn over my private data to web service companies. I’ve also been thinking about privacy in general, and while Canadian laws tend to protect consumer privacy to a greater degree than in the United States, I’ve concluded that I’m no longer happy using services where my private communications can be mined, sold, re-used, or thrown away when the service reaches the end of its life. That’s why I decided I would stop using Google for email and search.

I started this process a couple of weeks ago by drawing up a list of features I couldn’t do without. Those features had to be comparable to Google to make this effort worthwhile; regardless of my privacy concerns, if moving was a hassle or if I felt I had to compromise on services or ease of use, there wouldn’t be much point in making a change. The features I considered were:

- Calendar syncing across all devices
- A fast, responsive web interface
- IMAP syncing across devices
- Two-factor authentication or app passwords
- Excellent archiving tools
- Excellent spam detection
- Conversation threading
- Respect user privacy (namely mine!)
- Doesn’t save search history
- Servers outside of the US

Eventually, this list led me to choose [Fastmail](http://www.fastmail.fm) for email and calendars, and [DuckDuckGo](http://www.duckduckgo.com) for search.

### Why Fastmail

![Fastmail Logo](http://cecily.info/uploads/2014/07/FastmailLG.png)

Switching email providers is more complicated than switching search engines. If you depend heavily on email making the decision to switch isn’t one you can make lightly. We (have to) use Outlook at work, so I don’t have a choice of platform for corporate communication, but for everything else I used Gmail for the last seven years. Google’s decision to shutter Reader was a serious blow, and that provided the push I needed to investigate other options. When companies provide services for free, its easier for them to decide to “sundown” those services (hi, Posterous!). I didn’t want to find myself in a similar situation with email.

[Fastmail](http://fastmail.fm) provides everything I was looking for in an email provider. Their web interface is sparse and uncluttered, but options are clearly labeled, which helps with recognition. [Fastmail](http://www.fastmail.fm)’s web interface even recognizes Gmail shortcuts, something I really only use to reply and compose messages, but shortcuts that are handy nonetheless.

![Fastmail Inbox View](http://cecily.info/uploads/2014/07/FMinboxLG.png)

[Fastmail](http://www.fastmail.fm)’s headquarters are in Australia, but they have servers in the United States. As an Australian company, they’re only subject to Australian law and are only required to turn over identifying information to Australian authorities. If you’re concerned about the NSA program to search every email that leaves or comes into the United States, [Fastmail](http://www.fastmail.fm) provides a bit of a safety net. Even though their servers are in the US, [Fastmail](http://www.fastmail.fm) maintains that while it’s possible the United States could convince Australian authorities to comply with surveillance attempts, it would be highly unlikely.

I’m glossing over a bit of detail here, so if this is important to you, check out Fastmail’s [privacy policy](https://www.fastmail.fm/help/legal/privacy.html) or the blog post where the company outlines what the [location of their servers means in terms of privacy and surveillance](http://blog.fastmail.fm/2013/10/07/fastmails-servers-are-in-the-us-what-this-means-for-you/).

### Pricing

[Fastmail](http://www.fastmail.fm) has a variety of [plans](https://www.fastmail.fm/signup/personal.html), ranging from a $10/year light plan that provides 250MB email storage, to a $120/year premier plan that gives you 60GB email storage, mail and calendar sync, the ability to use your own domain, and priority support. I opted for the $40/year enhanced plan that comes with 15GB of email storage, mail and calendar sync, and the use of my own domain. If you’d like to try the service, Fastmail provides a free 60-day trial at the enhanced level. All of Fastmail’s plans are ad-free.

### Mail Setup and Migration

I wanted to see how well I liked the service before shutting down my Gmail account entirely, so I created a personality (what Fastmail calls accounts) that would let me send mail from Fastmail, but have it appear like it comes from my Gmail address. In the two weeks since switching to Fastmail, I hadn’t experienced any problems with this setup until a couple of days ago, when mail sent via Gmail started bouncing back as undeliverable. After trying — and failing — to get a response from Fastmail via Twitter, I quick web search turned up an easy solution that only required toggling a switch in my settings. So far, so good.

Migrating mail from one service to another can be a challenge, particularly if you have to rely on proprietary import/export mailbox formats. Fortunately, Fastmail makes migration painless; all I had to do was enter my Gmail user name, password, and server settings, and their migration script moved 15,000 messages in just under four minutes. I opted to import my mail using the same mailbox structure I used in Gmail, but you can choose to import the messages directly into your Fastmail inbox, if you prefer.

Spam hasn’t been an issue since moving from Google to Fastmail. I haven’t received a single piece of spam since switching. Not one. I still can’t quite believe it.

Fastmail provides fast, dependable email service. They won’t mine your messages so they can serve targeted advertising, they promise to protect your privacy, and the service makes migration, managing multiple accounts, and setting up mail for your domain easy. Documentation is plentiful and easy to understand although support could be more responsive. Because you pay them to manage your mail, it’s in their interest to offer a secure, seamless, and reliable experience, and I believe they’ve done that.

## Why [DuckDuckGo](http://www.duckduckgo.com)

![DuckDuckGo Logo](http://cecily.info/uploads/2014/07/DuckDuckGo-Logo.jpg)

I’m a librarian who would rather enter terms into a simplified search box and retrieve “good enough” results for my personal use instead of using the library’s (excellent and vast) collection of resources. Convenience is important to me, and despite my search skills, if I can use a simple search and near-natural language to return a list of serviceable results, I’ll reach for a search engine over a database nine out of ten times.

Search Results

The results from [DuckDuckGo](http://www.duckduckgo.com) are for the most part useful, and in the two weeks since I’ve used it as my primary search engine, I’ve been pleased with the results. It’s taught me a couple of things: (1) Google’s search history and tracking shaped my expectations around the relevance of search results more than I previously thought, and (2) I’m willing to trade increased relevancy for results that are free of spam and advertising.

[DuckDuckGo](http://www.duckduckgo.com) provides instant answers, maps, image searching, and integration with Wolfram Alpha. If you type weather into [DuckDuckGo](http://www.duckduckgo.com), the search engine will retrieve a five-day forecast for your location (I use this more than you might think, especially during a heat wave). If your search term has a related Wikipedia entry, [DuckDuckGo](http://www.duckduckgo.com) shows the results at the very top of the list and provides a list of links to related entries.

![Duck Duck Go's Search Results](http://cecily.info/uploads/2014/07/intersectionality_at_DuckDuckGo.png) 
*DuckDuckGo’s results for intersectionality – note the helpful related topics*

![Google's search results](http://cecily.info/uploads/2014/07/intersectionalityGoogleSearch.png)
*Google’s results for intersectionality contains a link and brief snippet of the Wikipedia entry.*

[DuckDuckGo](http://www.duckduckgo.com) is a fraction of the size of Google, and as a small team they can’t rely on a large team of engineers to build their search engine. Instead, the company uses their own web crawler to create an index based on hundreds of sources. It’s a comparable Google alternative and its principled stand on privacy pushes it to the top of the list as the search engine I’d most likely recommend to patrons…if I still had a job where I interacted with patrons.

Unlike switching email services, changing search engines will cost you nothing in terms of productivity. If you’re unconvinced, try [DuckDuckGo](http://www.duckduckgo.com) for a week and draw your own conclusions.

## What’s Next?

Over the next little while, I’ll try using [Fastmail](http://fastmail.fm)’s calendar as a replacement for Google Calendar, and I’ll look into finding a replacement for Google Drive and Google Hangouts. I take part in a few distributed committees that depend on Drive and Hangouts, and while I don’t feel hindered by this, I am reluctant to use any other email address with Google’s services. The team I manage uses Google Hangouts/Google Talk for communication, and I don’t want to be the kind of manager that foists change upon them for no reason other than my discomfort.

If you’re looking to switch services, start with something simple like switching search engines. I recognize that deciding to switch from a free email service to paid email hosting isn’t a decision that everyone can make lightly. However, if you care about your privacy, I wouldn’t recommend switching from one free email service to another. If you are using free services to send sensitive information of any sort, you’re granting these services permission to use your information in any way they see fit, from serving advertisements to cooperating with forced surveillance demands from government agencies. Weigh the benefits of convenience against privacy.

Lastly, If you’re a library worker, consider changing the default search engine on public workstations to [DuckDuckGo](http://www.duckduckgo.com) instead of Google, Yahoo! or Bing. When teaching patrons how to sign up for email accounts, consider adding information about privacy and managing your digital footprint to the course syllabus, and help your patrons understand how to make more informed decisions about who to trust with their communications.