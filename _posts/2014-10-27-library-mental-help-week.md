---
layout: post
title: "Library Mental Help Week"
share: true
comments: true
modified:
categories: 
excerpt:
tags: []
image:
  feature: mental-header.jpg
date: 2014-10-27T12:40:39-07:00
---

The week of October 27, is [Geek Mental Help Week](http://stuffandnonsense.co.uk/blog/about/announcing-geek-mental-help-week), an event that raises awareness about mental health in the web industry through conversations, magazines, blogs, and podcasts. As a public librarian, I've seen first hand the impact of poor support for mental health can have on a city's population and its library patrons, but I don't think we talk enough about how mental health affects library workers, either directly or indirectly. I'd like to change that. 

I propose that we start a **Librar*[^1] Mental Help Week**. Sometime in November (or December?) library-related publications, blogs, podcasts would devote time to discuss mental health in the profession. Here's how you can help: 

* Help plan the event
* Write about mental health issues on your own blog
* Create a website that will collect related posts (I'm willing to do this via Github Pages, but I could use a designer)
* Devote an episode of your podcast to mental health in the profession
* Organize a local event to talk about mental health issues
* Publish articles about mental health in professional journals or on their websites

I've struggled with depression and anxiety for most of my life, and try as I might, I can't always leave those emotions outside the library when I report to work. I've tried to be stoic, to power through the bad days by keeping my head down and not interacting, but that only adds to the workplace tension I create by being in the office. __Stoicism and evasion aren't healthy coping strategies__. They aren't sustainable, and I fail, often. 

Over time I've learned that I'm not the only one with a problem, society has a problem with mental illness, and wishing it away or pretending it doesn't exist only makes the problem worse. Like Paul Boag writes in [Smashing Magazine](http://www.smashingmagazine.com/2014/10/27/you-are-not-a-machine-you-are-not-alone/) we are not machines, and __we are not alone__. Together we can work to change the stigma of mental health in the library profession. 

[^1]: I intentionally use the wildcard because the event won't only focus on the health of credentialed librarians. All library workers are impacted by mental health in the workplace.