---
title: "Of what had I ever been afraid?"
layout: post
share: true
comments: true
---
The librarian twittersphere has been (rightly) up in arms about a number of things having to do with gender, sexism, intersectionality and privilege over the last few days. Some discussions have pushed a number of us right over the edge, causing outbursts like the one I uttered this morning: 

<blockquote class="twitter-tweet" lang="en">
  <p>
    Jesus God, librarians make my ass itch sometimes. And you can quote me.
  </p>
  
  <p>
    &mdash; Cecily Walker (@skeskali) <a href="https://twitter.com/skeskali/statuses/417699117855297538">December 30, 2013</a>
  </p>
</blockquote>



In my frustration, I somehow overlooked that this shared outpouring of words, thoughts, and emotions was not only very necessary, but healing. I felt connected to, heard, and validated by these women and men who stood up to say &#8220;Not on my watch,&#8221; or &#8220;Here&#8217;s where you&#8217;re wrong,&#8221; or even simply &#8220;We tried. We have a long way to go. We welcome dialogue.&#8221;

It reminded me of something Audre Lorde once wrote in her essay &#8220;The Transformation of Silence into Language and Action&#8221;: 

> &#8220;(F)or every real word spoken, for every attempt I had ever made to speak those truths for which I am still seeking, I had made contact with other women while we examined the words to fit a world in which we all believed, bridging our differences. They all gave me a strength and concern without which I could not have survived intact&#8230;Within the war we are all waging with the forces of death, subtle or otherwise, conscious or not &#8212; I am not only a casualty, **I am also a warrior**.&#8221;

I have frequently felt out of place in this profession, felt too coarse, too brash, too outspoken, too unrefined, too, too, *too*. And what I&#8217;ve come to realize is that these little arrows of insecurity and vulnerability are part of a much larger skirmish that I&#8217;ve fought my entire life against a society that not only made me feel unwelcome, but that had little interest in my success or survival. 

This is familiar, this battle. I have seen it all before, and I have &#8212; **we have** taken on bigger foes than those that would end us, and we have won. 

We are so powerful. We can do so much. 

Thank you for pulling my coattails and reminding me just how full of the right stuff we are. 