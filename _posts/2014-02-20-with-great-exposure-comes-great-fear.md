---
title: With Great Exposure Comes Great Fear
layout: post
share: true
comments: true
---
I have been struggling with the amount of attention (and requests for engagement) that have come my way as a result of a couple of blog posts I wrote about intersectionality in librarianship. In an attempt to understand why I felt like I was slowly having the top layers of my skin flayed off, I started reading Brené Brown&#8217;s *Daring Greatly*. 

You know how you can read a text and find so much that resonates with you that the whole thing glows with highlighter marks and is littered with post it notes? If I were reading a paper copy, this would be one of those books. Alas, it&#8217;s an ebook. 

Anyway, Brown, if you aren&#8217;t familiar with her work, focuses on the ways that shame and a fear of vulnerability keeps us from achievement, whether in personal or professional contexts. Early in the book she relates the story of her journey toward accepting the increased attention she received as a result of her TEDxHouston talk. Her inclination had been to shrink from the spotlight, not because she didn&#8217;t believe in her research, but because she was afraid to make herself vulnerable. She writes:

> In a culture full of critics and cynics, I have always felt safer in my career flying under the radar.

Seeing this in print had such an effect on me that I couldn&#8217;t be bothered to get out of bed and hobble over to my computer, I had to immediately peck this entry out on my iPhone. 

I am as critical and cynical as the next person. That may be why I was attracted to UX work in the first place, and why I feel so comfortable with my position as the Merry Snarkster in #LibrarianTwitter. But if I&#8217;m being honest &#8212; and I think it&#8217;s time I do that for once &#8212; the level of cynicism I&#8217;ve seen on Twitter, blogs, and library publications of late is disturbing. It&#8217;s disappointing, and disheartening and the idea of opening myself up to these well-honed slings and arrows scares the *shit* out of me. 

It isn&#8217;t because I don&#8217;t believe in what I have to say, or because I believe it isn&#8217;t important. I just don&#8217;t want to invite the negativity into my life, especially now when I feel ill-prepared to handle it in a mature and emotionally detached fashion. 

I think the cynicism is dangerous, and I think it silences just as many people as the fear of racist, sexist, or homophobic reprisal does. 

I don&#8217;t have any answers, and obviously, this is something I&#8217;ll have to keep working on in my own life, but at the risk of losing an audience who expects snark and sass from me, I&#8217;m going to do my best not to contribute to the bitter back channel anymore. 

And while I&#8217;m at it, I&#8217;ll also work on stepping out from behind a persona and allowing you to see who I **really** am, scars, warts and all.