---
title: Neil deGrasse Tyson on Museums (and Possibly Libraries?)
layout: post
share: true
comments: true
---
> How long does a person stay at a museum? A couple of hours? There are people who would want exhibits at a museum to have a whole lesson plan so you can poll people and ask them &#8220;What did you learn?&#8221; Then you&#8217;d judge the success of the exhibit based on how well people do on these exams.
>
> I have a different view. The person is going to spend incalculably more time in a classroom than they ever will in a museum. So a museum shouldn&#8217;t be a supplement to a classroom. It should be a force to ignite flames within a person&#8217;s soul of curiosity. An exhibit should make a person say &#8220;Wow! I&#8217;ve got to find out more about this!&#8221; and trigger them to explore more advanced accountings of the topic, in books or science videos. Once the flame is lit, the learning becomes self-motivating. &#8212; (from &#8220;<a href="http://io9.com/neil-degrasse-tyson-explains-why-the-new-cosmos-matters-1534876280" target="_blank">Why The New Cosmos Matters</a>&#8220;)
