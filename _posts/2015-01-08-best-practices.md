---
layout: post
title: "Best Practices"
share: true
comments: true
modified:
categories: 
excerpt:
tags: []
image:
  feature:
date: 2015-01-08T12:32:54-08:00
---

I don't often go on a tear about UX or library UX, but it's a new year, and I feel like maybe it's time to break with conventions. And speaking of breaking with conventions...

**Kill your best practices**.

See what I did there? 

Someone who is much smarter than I am once said "Best practices are for people who can't think for themselves." These conventions become standards because people grow used to them. It does not mean that the interaction is the most efficient, elegant, or even the best designed. What had happened was[^1] somewhere along the way an interaction designer had an idea for a system response. Another IxD saw this design, thought "Hey, that looks pretty good, why don't I just use that?" and in no time at all, every single website had its logo in the upper left hand corner[^2]. 

Best practices and conventions are good *guidelines*. They're a starting point for a design conversation, not the entire dialogue. At the risk of doing the very thing I dislike[^3], I'm going to step out on a limb and say that if you're simply accepting best practices without testing them with your own user groups, you're part of the problem and you should just stop. **Stop**. Right now. Walk away from OmniGraffle, move away from Visio, and just..I don't know, go for a hike or something. And while you're on a hike, take a mobile-friendly version of your wireframe with you so you can test your design with someone on the hiking trail.

[^1]: This is intentionally poor grammar. 

[^2]: Not all conventions are bad, they're just unexamined.

[^3]: Ponderously pontificating about design when I'm not a designer, but w'ev.