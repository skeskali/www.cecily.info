---
layout: post
title: "Preparation Peril"
share: true
comments: true
modified:
categories: 
excerpt:
tags: []
image:
  feature:
date: 2015-01-08T01:04:36-08:00
---

On Tuesday I delivered a webinar[^1] for [LITA](http://www.ala.org/lita/ "Library Information Technology Association") on creating web maps with the [Leaflet](http://leafletjs.com) JavaScript library. I'm afraid the session didn't go exactly as I planned. The problem wasn't the content, it was in my execution and delivery. In working so hard at making sure the session was content-rich, I forgot about the importance of performance[^2]. 

When I'm in front of an audience I can see, I adapt my presentation style based on visual and verbal cues the audience gives me. An online seminar removes all of that useful feedback, and if I can't tell how I'm doing, I lose the plot, and when that happens, it's hard for me to pick it up again.  

I felt like I was talking to myself in an empty room, which of course I was[^3]. I wasn't prepared for how this format would change my delivery. I was nervous, I didn't feel engaged with the topic, and I certainly didn't feel engaged with the audience, through no fault of their own. It just *didn't work*, and being the perfectionist I am, I haven't been able to let this go.

If I ever do another seminar in this format, I'll do several things differently:

* Write a shorter script (my script was approximately 8 printed pages long, not including screenshots)
* Deliver the seminar to a live audience, solicit feedback, and leave enough time to incorporate any changes.
* Practice, practice, practice
* Stick to subjects I'm (very) familiar with
* Use fewer examples with greater detail

Of course, it's entirely possible that the real lesson in all of this is I work better with a live audience, and I should just stick to what works...


[^1]: I hate this word with a white-hot intensity. I'll use it only once in this entry. My apologies.

[^2]: My dearly departed mentor Jeffrey Woodyard was instrumental in helping me develop an understanding of performative pedagogy when I was an undegraduate student. I call on his memory whenever I step behind a podium.

[^3]: Unless you count my cat, and she wasn't interested *at all*.