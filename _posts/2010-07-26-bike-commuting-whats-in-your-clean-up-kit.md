---
title: Bike Commuting&#58; What's In Your Clean-Up Kit?
comments: true
share: true
layout: post
dsq_thread_id:
  - 122502189

---
If you've been following along, you probably know I decided to try commuting to work by bike this summer (and maybe year round, provided that I can get good gear that fits). Thankfully the library is a pretty casual place when you're an entry-level librarian, so I wear my regular work clothes when I ride even when it's hot out. But I don't want to stink up the office after I arrive, so I'm always looking for good products to keep in my clean-up kit in my locker.

Most bike commuters will swear by baby wipes and dusting powder to help you feel and smell a bit more fresh, and I'd recommend the same. If I quickly dab the sticky/sweaty spots as soon as I get to work and lightly dust on some body powder, the pong is whipped into submission and I feel fresh all day long. Some folks will tell you baby powder is a good substitute, and it can be provided that it's made of cornstarch, but Shower to Shower is my dusting powder of choice because it's the most absorbent powder I've ever found, and I don't smell like a baby after I've used it. Deodorant/Antiperspirant is a given, although part of me is thinking of switching to a natural deodorant because I don't actually sweat all that much under my arms (TMI!).

If you're not of the African American persuasion or not into rockabilly, you might be wondering what the little orange tin is for. If you're black, you probably already know what this is, so you can skip the next two paragraphs. This is a tin of Murray's Hairdressing, the strongest hair pomade I could find in Vancouver, and it's something I remember my dad and brothers using on their hair to keep it neat and tidy. Why is it a part of my clean-up kit? See, I have dreadlocks, and while having natural hair means I don't have to worry about my hairstyle going back[^1] when I get all hot and sweaty under my helmet, the edges of my hairline go all fuzzy and that can make me feel a little unkempt. Nobody at work has ever said anything negative about my dreads, but because I choose not to straighten my hair, and because I'm the only black female librarian in the whole system, I feel compelled to make sure my hair looks well-groomed even when I'm dressed somewhat casually. That's where the pomade comes in.

When I get to work, I dampen the hair at my hairline. I take a tiny bit of Murray's, rub it between my fingers to warm it up, and apply it to my hairline. Then, taking a stiff smoothing brush, I brush my hair back at the temples until it is sleek and smooth. No more fuzzy edges! I've tried other pomades; while they work OK, my fuzzy edges usually return by mid-afternoon. Not so with Murray's. This stuff is powerful.

As for the dreads themselves, I've pretty much given up trying to wear my hair in [pipe cleaner curls](http://www.youtube.com/watch?v=lX-opEkZTQM) because as soon as I start to work up a sweat, my hair won't hold a curl. I usually wear my hair down and tuck it behind my ears while it's under the helmet, but when I get to work, I use stretchy headbands, hair pins, or hair ties and pin my hair up in a messy up-do. I'm a librarian, so I try to stay away from the whole librarian-in-a-bun look, but dammit, a bun is functional when you don't want to walk around with boring lifeless hair. The alternative is shaving off my dreads and going back to a teeny weeny afro, but I have a big round basketball head (on top of a big round basketball body), so I'm not too eager to go back to wearing super short hair just because it would be more efficient.

So there you have it &#8211; my bike commuting clean-up kit. What sort of things do you use to clean up after your ride to work? Do you have any unusual products in your kit that help make your commute more bearable?


 [^1]: straightened hair that reverts to its natural, kinky state
