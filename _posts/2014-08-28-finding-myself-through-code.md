---
layout:     post
comments:   true
title:      Finding Myself through Code
date:       2014-08-28
share:      true
summary:    Or how I found self-confidence through web development.
---

Just a note to let you know that you may experience some downtime and/or 404 not found messages while trying to access this content over the next little while. I’ve decided to move away from WordPress and give hosting a static website a try.

Why am I doing this? It has nothing to do with WordPress; not really. I (mostly) enjoy working with WordPress and will use it for many other projects in the future, but I need to challenge myself as a budding developer, and deploying static websites using [Jekyll](http://jekyllrb.com) is one such challenge I’ve taken on.

So far, I’ve managed to run the site for [Maptime Vancouver](http://maptimeyvr.org) on Github Pages using Jekyll. Even though the configuration process left me scratching my head and swearing a few times, I enjoyed the work and, perhaps more tellingly, *I loved how that success made me feel*. Instead of running away the moment I ran into a problem I couldn’t solve, I persevered and eventually worked my way through a solution.

Somehow I managed to make it through this long life by sidestepping most intellectual and professional challenges that came my way. I’ve learned that I didn’t do this out of feelings of inadequacy, I did it because I allowed myself to create a world where I’m afraid to fail. Failure wasn’t an option for me, I thought, because as a black woman, I had to be twice as good as anyone else if I expected to get half as far. So rather than putting in the hard work, I checked myself out of the race, out of life and experiences. I let myself believe that I was okay accepting less.

Eventually the lie caught up to me.

I grew tired of hitting professional roadblocks and recommitted to strengthening my code skills. Thanks to a boss who has known me for a long time (and is probably long tired of my bitching and questioning), opportunities were recently created that allowed me to put some of my code skills to the test. More amazingly, even when the project went sideways, I didn’t view it as a failure. It’s the worst type of business-writing cliche there is, but this “failure” became a learning opportunity that revealed strengths and commonalities instead of reinforcing divisions and silos.

I’m learning. I’m growing. There are possibilities all around me, and my brain is crackling with excitement. Sure, I swear a lot more, and my brow may be permanently furrowed, but trust me when I say I am blissfully happy to be doing what I’m doing.

This was a long-winded way of saying that if you’re looking for specific entries on this website, you may not find them for a few days (hopefully not weeks).  You might be mildly inconvenienced, but hopefully the mental image of yours truly happily hacking away in the background will extend your patience.

Thank you.