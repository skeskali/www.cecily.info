---
title: 'Calling In: A Less Disposable Way Of Holding Each Other Accountable'
layout: post
share: true
comments: true
---
> When confronted with another person’s mistake, I often think about what makes my relationship with this person important. Is it that we’ve done work together before? Is it that I know their politics? Is it that I trust their politics? Are they a family member? Oh shit, my mom? Is it that I’ve heard them talk about patience or accountability or justice before? Where is our common ground? And is our common ground strong enough to carry us through how we have enacted violence on each other?
> 
> I start “call in” conversations by identifying the behavior and defining why I am choosing to engage with them. I prioritize my values and invite them to think about theirs and where we share them. And then we talk about it. We talk about it together, like people who genuinely care about each other. We offer patience and compassion to each other and also keep it real, ending the conversation when we need to and know that it wasn’t a loss to give it a try.

A couple of people mentioned (on Twitter) that they couldn&#8217;t believe how calm I was in the [intersectionality post][1] and in the conversation that spurred it. I used a feminist twitter meltdown as a model of behaviour I didn&#8217;t want to follow, but it was the piece &#8220;<a href="http://www.blackgirldangerous.org/2013/12/calling-less-disposable-way-holding-accountable/" title="Calling IN" target="_blank">Calling In: A Less Disposable Way Of Holding Each Other Accountable</a>&#8221; by Ngọc Loan Trần that also helped me to keep my reactions in check. 

(Also: <a href="http://www.blackgirldangerous.org" title="Black Girl Dangerous" target="_blank">Black Girl Dangerous</a> should be required reading.)

 [1]: http://cecily.info/2013/12/20/on-privilege-intersectionality-and-the-librarian-image/ "On Privilege, Intersectionality, and the Librarian Image"