---
title: Don't Get Angry, Get Crunk
layout: post
comments: true
share: true
date: 2012-09-03
---
This is <a href="http://www.realclearpolitics.com/video/2012/09/02/msnbcs_harris-perry_snaps_i_am_sick_of_the_idea_that_being_wealthy_is_risky.html" title="MSNBC's Harris-Perry Snaps: &quot;I Am Sick Of The Idea That Being Wealthy Is Risky&quot;" target="_blank">one of the greatest moments in television punditry</a>. Melissa Harris-Perry goes off on a panelist who dared to make the comment that being wealthy is riskier than being poor. That moment reminded me of a quote I read in a piece by the Crunk Feminist Collective:

> One of the ways White supremacy and sexism works is through a putative disavowal of emotion as a legitimate form for expressing thought. Women and Black people are overly emotional, so the conventional wisdom goes. We have been taught to overcompensate for this stereotype by being overly composed, even when anger is warranted. And we are wholly unprepared when our emotions start to split the seams of our tightly put on public selves. Perhaps it’s time to change clothes, and intentionally put on something that gives us room to breathe.
> 
> For me, that has meant embracing my own crunkness. <a href="http://crunkfeministcollective.wordpress.com/2012/09/03/at-the-risk-of-sounding-angry-on-melissa-harris-perrys-eloquent-rage/" target="_blank">Why go off when I can GET CRUNK</a>? And by that I mean I can make an intentional choice to use my legitimate and righteous anger in an honest and compassionate way that is potentially transformative. 
> 
> &#8211; <a href="http://crunkfeministcollective.wordpress.com/2012/09/03/at-the-risk-of-sounding-angry-on-melissa-harris-perrys-eloquent-rage/" target="_blank">The Crunk Feminist Collective</a> 

This is something I struggle with quite a bit at work. I&#8217;m one of two black librarians at MPOW, and sometimes I think what some colleagues read as anger &#8212; because I don&#8217;t frame my arguments in a dispassionate, remote manner, and because my true feelings are always written all over my face &#8212; is read incorrectly because it&#8217;s different, because *I&#8217;m* different. We talk about diversity in the workplace, but it seems that there still is only one right way to present a position, and more often than not, I feel as if I never get it right. I wonder if I were in an environment where my minority status wasn&#8217;t quite so obvious if I&#8217;d still face the same challenges, or if there would be enough cultural shorthand and similarity going around where people could shrug off my &#8220;passion&#8221; and get to the heart of what I&#8217;m saying. I don&#8217;t suppose that&#8217;s something I&#8217;ll ever know, what with living in Vancouver and all.

So instead of getting angry, I&#8217;m going to learn to GET CRUNK about the changes I&#8217;d like to see. I&#8217;m going to learn to harness eloquent rage, even at the risk of alienating others. Part of this process of learning to evangelize myself is learning how to speak the truth without fear.