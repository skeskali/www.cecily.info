---
layout: post
title: "Scenes From a Bike Lane"
modified:
categories:
excerpt:
tags: []
date: 2015-03-07T16:52:15-08:00
---

I felt my lungs expanding, the tension leave my body. I've been away from my bike for much too long, and although last week's aborted turn left me in poor spirits, today's abundant sunshine and early-spring warmth lured me back to the saddle. The sun was both blinding and restorative, and I felt the sap begin to flow through my creaky joints. I pointed my bike north and east with no particular destination in mind, but was very happy to find myself headed toward one of my favourite watering holes. I didn't dally; I stayed only long enough to enjoy one small beer and to make a sadly necessary phone call to my cellular provider.

![West Hotel on the Carrall Street Bike Route](http://cecily.info/images/carralstreet.jpg)

I pedaled south toward home along the Carrall street bike route, and stopped at the red light. A man sat just to the right of the lane in a camp chair, a large duffel bag at his feet, and a baseball cap perched insouciantly on his head. He called out:  "Nice bike, mama!", and since the compliment was about my bike and not about me, I thanked him. We talked about how lovely the day was, how it was great to see so many people out and about again after a long (yet unseasonably warm) winter, and how the little pleasures, like riding a gorgeous bike on a beautiful day, or finding a thoughtful spot where you could read a book meant so much, and were overlooked by so many. I asked him what he was reading; I'm a librarian whose interested in people, so I almost always ask. Sadly I can't remember the title, _Yakuza_...something. About an Iranian in Japan. 

He told me a little about the time he spent 7 weeks in Japan working as a bartender, hanging out with people who were strippers &amp; iand gangsters from Japan's criminal underworld. In the middle of telling this story, he paused, looked me in the eye and said "It's nice to have a conversation. We're bringing it back."
