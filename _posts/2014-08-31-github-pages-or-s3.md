---
layout: post
title: Github Pages or S3?
published: true
share: true
comments: true
summary: In which I mull over my flat file CMS choices.
---
Jekyll is starting to become more familiar as I spend more time working with it, yet having to build pages then upload them to S3 feels like a step back, not an enhancement. 

I only have a very small site with very few visitors. Do I really need the speed and power of Amazon S3, or would hosting these pages on Github Pages be sufficient? Here are the challenges I'm facing with S3 hosting: 

- Pages being out of sync between dev environments and not having the skill to keep them in sync (git maybe?)
- Not being able to edit everywhere

Without a doubt, AWS and Cloudfront are blisteringly fast, but I am not so concerned with speed and performance that I will notice any significant difference between Github Pages and S3. 

Challenges, challenges, rise to the challenge. I'd like to think I *am* doing that, but at some point I have to admit to myself that convenience is *mighty alluring* and isn't anything to sneeze at.