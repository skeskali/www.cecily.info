---
title: My First Launch Center Pro Action
layout: post
share:  true
summary: In which I experiment with creating a Launch Center Pro action for Day One.
---
I'm a huge fan of <a href="http://dayoneapp.com" title="Day One for iOS and the MacOS">Day One</a>, the journaling app for iOS and MacOS X. Even though I&#8217;m not a very disciplined journaler (or blogger), Day One makes it really easy to quickly capture work ideas, stray thoughts, crazy dreams from the night before, and just about anything else I might want to refer to later.

One of the ways my journaling practice has improved is by using automated actions to create a template for a daily summary entry. If you&#8217;re using iOS, there are many options available for automating actions, managing snippets and building templates but one of the most powerful is <a href="http://contrast.co/launch-center-pro/" title="Launch Center Pro" >Launch Center Pro</a>. In brief, Launch Center Pro doesn&#8217;t just launch apps, it allows you to launch complex actions in a single click on your iPhone or iPad.

<a href="http://jwie.be/writing/launch-center-pro-daily-journaling/" title="Josiah Wiebe - Launch Center Pro and Daily Journaling" >Josiah Wiebe</a>&#8216;s Launch Center Pro Daily Summary action is great for quickly entering data into Day One, but I didn&#8217;t like the rigid table formatting, and I needed a few extra options to help me keep track of days off (sick days, vacation days, days when the library is closed, and so on). So, armed with the original action , a text editor, and a Wikipedia entry that explained percent encoding, I created my first <a href="https://launchcenterpro.com/zhqjpf" title="My First Launch Center Pro Action!" >Launch Center Pro daily journaling action</a>.

The formatting differences between Wiebe&#8217;s action:
![Josiah Wiebe's Launch Center Pro Action](/uploads/2014/06/2014-06-28_21-21-24.png)


and my version:  
![Cecily's Launch Center Pro Action](/uploads/2014/06/2014-06-28_21-28-05.png)

I may revisit this action at some point in the future. I&#8217;d like to create an action for tracking my rheumatoid arthritis flare ups so that I can understand (and maybe predict) them better.

This is the very first time I&#8217;ve shared anything that sort of resembles code or an automation on my blog, and I&#8217;m pretty excited about it. As I wrote in my journal, all of the work (and money!) I&#8217;ve put toward working with Ruby, JavaScript, and WordPress is changing the way I think about writing code and how I use code and automations to solve problems.

<a href="https://launchcenterpro.com/zhqjpf" title="Simple Daily Summary for Day One - requires Day One and Launch Center Pro" >Download my Launch Center Pro Simple Daily Summary action for Day One</a>.
