---
title: Advice from a Sparklepony -  Breaking Through Silos
layout: post
comments: true
share: true
date: 2013-01-25
---
[<img src="http://farm9.staticflickr.com/8181/8055405790_d4a4e30849_z.jpg" width="640" height="360" alt="Fashion Style Twilight Sparkle" />][1]

Let me explain the title. 

At last year&#8217;s <a href="http://www.infotoday.com/il2012/" title="Internet Librarian 2012" target="_blank">Internet Librarian conference</a>, I made a comment about being a &#8220;sparklepony&#8221; in your library. What I meant was that if you&#8217;re someone who has a special skill that no one else on staff has, you should evangelize on your own behalf, stop downplaying that expertise just to make others feel comfortable or less threatened, and make sure that you use your skills in the service of others.

I mentioned my Sparklepony Theory of Professional Success &trade; during a presentation I made to staff last week about the Digital Public Library of America (<a href="http://dp.la" title="DPLA" target="_blank">DPLA</a>) Appfest that I attended last November. Part of DPLA&#8217;s mission is to break down the information silos that exist between libraries/cultural institutions and their digitized collections. 

These silos also exist in libraries, particularly among library staff who create artificial divisions between &#8220;techie&#8221; and &#8220;non-techie&#8221; staff members. I am a web services librarian, and you are a children&#8217;s librarian, and neither of us can ever be anything else ever, ever, ever, The End. 

**That&#8217;s malarkey.** 

During my talk, I made a point of telling the audience that **they could break through these silos if they really wanted to** by looking for other ways to become involved in technical projects. My own work experiences on this front have been frustrating as all get out, and there were many times I thought about leaving the library. But instead of giving up, I looked for other ways to do the work I wanted to do. I paid for a <a href="http://www.lynda.com" title="Lynda.com - online learning" target="_blank">Lynda.com</a> subscription and took many courses on WordPress, Drupal and PHP, and I&#8217;ve made a few introductory forays into the courses on offer through Codeacademy. I taught a couple of courses on WordPress, usability, and human factors, and I read a lot of blogs. I reached out to developers who were affable and approachable, and made contacts with some librarians who were all too willing to answer my annoying newbie questions. I put in the work on my own time, but it paid off. 

Here are a few suggestions I made for ways to break through: 

*   Work on <a href="http://www.codeacademy.com" title="Code Academy - go now." target="_blank">Codeacademy</a> lessons during your lunch break at your desk
*   Read bloggers like <a href="http://andromedayelton.com/" title="Andromeda Yelton - Across Divided Networks" target="_blank">Andromeda Yelton</a>, <a href="http://www.bohyunkim.net/blog/" title="Library Hat" target="_blank">Bohyun Kim</a>, <a href="http://yobj.net/notablog/" target="_blank">Becky Yoose</a> and <a href="http://exitpursuedbyabear.net/" title="Exit Pursued By A Bear - Lisa Rabey" target="_blank">Lisa Rabey</a>, just to name a few
*   Write about your struggles and your successes. GYODB[^1], but comment on others.
*   Get on Twitter. **Now.**
*   If you can&#8217;t write code, write documentation. It&#8217;s every bit as important, and very necessary.

What I most wanted to do in the talk was to convince the staff that there&#8217;s a way in for them as well, that *the Sparklepony mystique is just a myth*. I may be a special unique snowflake in my organization &#8212; and that&#8217;s hella debatable &#8212; but I don&#8217;t take any particular pride in it, and they shouldn&#8217;t be intimidated by it. I am by no means an expert developer. To call myself a developer would not just be a stretch, it would be a lie. But what I am is **code literate**. And any librarian who is willing to put in the time, to make connections, and to look for a way in &#8212; even on her own time &#8212; can do the same. 

Being a lone Sparklepony isn&#8217;t much fun. I want others around me who can frolic, play, break stuff, and make stuff better. 

Join me, won&#8217;t you?

 [1]: http://www.flickr.com/photos/rjrgmc28/8055405790/ "Fashion Style Twilight Sparkle by rjrgmc28, on Flickr"
[^1]: get your own damn blog