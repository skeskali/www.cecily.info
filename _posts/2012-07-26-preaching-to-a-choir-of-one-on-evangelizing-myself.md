---
title: 'Preaching to a Choir of One: On Evangelizing Myself'
layout: post
dsq_thread_id:
  - 781687506
comments: true
share: true
date: 2012-07-26
---
A while back, a colleague and I submitted a proposal for [this year&#8217;s Internet Librarian][1] conference in Monterey, CA. We&#8217;d planned to talk about creating hackerspaces at libraries, but the organizing and review committee had a different (and better) idea. Instead, we&#8217;ll be appearing on a panel of experts titled &#8220;Transforming Roles: Who Do You Want to Be?&#8221; We&#8217;ll discuss the changing roles of information professionals, how we ended up where we are, and the risks, rewards, and challenges of our journeys. Very cool, yes? I thought it was, so I told the organizers I&#8217;d be delighted to participate. 

Last week I found out that the panel is a Tuesday night session. If you don&#8217;t know anything else about Internet Librarian, know this: the Tuesday Night sessions are very well attended, are usually some the most popular, non-keynote sessions of the conference, and is a marquee spot. Not only that, but my name is on the front page of the website. 

Now, if you know me at all, you probably know that I lack confidence. I have enough self-esteem issues to stock a newsstand. Sure, I know and have learned a lot about user experience in the years since I graduated from library school. I&#8217;ve even been paid to teach college courses on it. That doesn&#8217;t change the fact that there&#8217;s a very loud, insistent gong of doubt that goes off in my head whenever I have to talk about myself or share what I know in person or in writing. I&#8217;m usually fine when I&#8217;m on stage, but it&#8217;s afterward that I want to curl up and die.

It only took me 25 years in the workforce to realize that being a wallflower isn&#8217;t conducive to professional success. 

![Who the hell am I and why should you listen to me?](http://cecily.info/uploads/2012/07/evangelizingyourself-090323104224-phpapp02.pdf-page-62-of-62-500x296.png)

These days I&#8217;m starting to speak up and express my opinions at work and online, even when those opinions run counter to the conventional wisdom. Most of the time it comes across like a bulldozer, but one step at a time. I&#8217;ll work on finesse later. I&#8217;m working hard on learning and practicing empathy and using non-confrontational language. 

A big part of this learning process is developing faith in my abilities &#8212; in myself. I&#8217;m learning to speak loudly and often about what I know, to share it with others, to ask questions, to listen, and to admit when I don&#8217;t know something but would welcome the opportunity to find out more about it. And yes, a big part of that is stopping this need I have to &#8212; if you&#8217;ll pardon the cliche &#8212; hide my light under a bushel. I am ready for the next big professional challenge that comes my way. OK, I&#8217;m really **not** ready, but I&#8217;m getting ready, and I know now what I have to do to become ready. The biggest challenge isn&#8217;t finding the perfect job, or even advancing in the organization, the next big challenge is me. 

[Whitney Hess&#8217; 2009 presentation on Evangelizing Yourself][2] came into my life at a moment of spiralling self-doubt.I secretly know I&#8217;m a little bit awesome, but I don&#8217;t want to brag, because no one respects a self-aggrandizing moron.

But if I don&#8217;t talk about myself, how in the world can I expect anyone to know anything about me? If I want to control the message that exists about Cecily Walker, I need to be sure that most of that messaging is coming from what I say and do well, not from people who I have wronged or disappointed in the past.

In her presentation, Hess challenged the members of the audience to explain why they&#8217;re important in one sentence without any hesitation. I&#8217;m going to do that now, but I reserve the right to edit the explanation at a later date. 

>I am important because empathy for others is the foundation of my personal and professional practice. 

It isn&#8217;t perfect, but it&#8217;s good enough for now. 

The key part of that sentence for others is the part after &#8220;because&#8221;. The key part for **me** comes before it. 


 [1]: http://www.infotoday.com/il2012/ "Internet Librarian 2012"
 [2]: http://whitneyhess.com/blog/2009/03/29/my-presentation-at-ia-summit-2009-evangelizing-yourself/ "Whitney Hess - Evangelizing Yourself"