---
layout: post
title: "Through the Lens"
date: 2015-01-25T11:35:22-08:00
---

I've let depression and illness keep me from one of my favourite hobbies for too long. Instead of lamenting what I can't do anymore (walk for hours while carrying heavy kit), I'm going to celebrate what I can (take photos of my surroundings with a much lighter camera).

![What's New Pussycat](http://cecily.info/images/20150125-P1250015.jpg)

After much deliberation, I settled on an [Olympus OM-D E-M10](http://thewirecutter.com/reviews/best-mirrorless-camera-under-1000/) micro 4/3 camera. It is so small and cute it reminds me of a puppy who is just learning to walk. Yet despite its small stature, it produces incredible photos with an amazing amount of detail, sharpness, and beautiful tones straight out of the camera (although I'll still do most of my editing in VSCO Film or VSCO Cam).

![Texture Play](http://cecily.info/images/20150125-P1250016.jpg)

This is one instance when saying "More to come" isn't just a stalling tactic. I don't know if I'll ever be as prolific a photographer as I once was, but I hope that being forced to slow down will make me a **better** one.
