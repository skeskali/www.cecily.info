---
layout: page
title: Archive
---

<ul class="post-list">
    {% assign current_year = "2013" %}
    {% for post in site.posts %}
      {% assign post_year = post.date | date: "%Y" %}
      {% if post_year != current_year %}
        <br />
       <h2>{{ post_year }}</h2>
      {% endif %}
      <li><article><a href="{{ site.url }}{{ post.url }}">{{ post.title }} <span class="entry-date"><time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%B %d, %Y" }}</time></span></a></article></li>
      {% assign current_year = post.date | date: "%Y" %}
    {% endfor %}
</ul>
