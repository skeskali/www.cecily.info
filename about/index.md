---
layout: page
title: About Cecily
excerpt: "A little bit about the author"
modified: 2014-08-08T19:44:38.564948-04:00
image:
  feature: rizzo-barn.jpg
---

I'm a librarian, wanna-be front-end developer, and well-intentioned curmudgeon. I love my bike (that's her in the header photo), my cat, and people, though not necessarily in that order. 

I was born and raised in Atlanta, GA, but I've called Vancouver, BC home for  14 years. I've been known to hug and kiss my Canadian passport in gratitude. 

I'm a member of the [American Library Association][1], and serve on the editorial board of [In the Library with the Lead Pipe][2], an open access, open peer reviewed journal of Library and Information Science. 

### About this Site

This site is powered by [Jekyll][4] and uses the [So Simple Theme][5].  I use [Disqus][6] for comments. 

[1]: http://www.ala.org "American Library Association"
[2]: http://www.inthelibrarywiththeleadpipe.org "In the Library with the Lead Pipe"
[4]: http://jekyllrb.com "Jekyll - Blog Aware Static Site Generator"
[5]: http://mademistakes.com/articles/so-simple-jekyll-theme/ "So Simple Theme by Michael Rose"
[6]: http://disqus.com "Comments in the Cloud with Disqus"
